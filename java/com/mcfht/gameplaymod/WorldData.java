package com.mcfht.gameplaymod;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldSavedData;

public class WorldData extends WorldSavedData
{
	protected NBTTagCompound data = new NBTTagCompound();	
	public boolean enabled = false;
	public WorldData(String tagName) 
	{
		super(tagName);
		enabled = true;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound) 
	{
	   	 data = compound.getCompoundTag(mapName);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound compound) 
	{
		compound.setTag(mapName, data);		
	}
}
