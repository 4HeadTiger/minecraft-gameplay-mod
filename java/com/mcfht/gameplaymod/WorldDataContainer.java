package com.mcfht.gameplaymod;

import java.util.Set;
import java.util.Map.Entry;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;

/**
 * Provides easy access into the world data
 * @author FHT
 *
 */
public class WorldDataContainer
{
	/** Whether this container will save data*/
	public boolean enabled = false;
	/** The ID to use for this NBT (should be unique) */
	public final String mapName;
	
	/** This object is what stores our NBT, and is saved into the world NBT on world save. */
	WorldData persistentDataHandler;
	
	/** Initializes the container. Just super this constructor with the desired unique ID and the system
	 * should do the rest.
	 * 	
	 */
	public WorldDataContainer(String id)
	{
		this.mapName = id;
		enabled = true;

	}
	
	/**
	 * When the world loads, if this container is enabled, it will be sent the NBT stored under
	 * the ID of this container. The NBT Compound may be empty.
	 */
	public void fromNBT(NBTTagCompound compound) {}
	
	/**
	 * This method is called when the world is saved. In this method, you should write the
	 * necessary data into the passed NBTTagCompound.
	 * @return
	 */
	public NBTTagCompound toNBT(NBTTagCompound compound) {
		return compound;
	}
}


