package com.mcfht.gameplaymod;

import io.netty.buffer.ByteBuf;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.world.WorldEvent;

import com.mcfht.gameplaymod.asm.ASMLoadingPlugin;
import com.mcfht.gameplaymod.asm.Days_ASMTransformer;
import com.mcfht.gameplaymod.daylength.Module_Days;
import com.mcfht.gameplaymod.daylength.TimeUpdatePacket;
import com.mcfht.gameplaymod.gui.hud.Levels_HUD;
import com.mcfht.gameplaymod.gui.hud.Levels_RespawnPacket;
import com.mcfht.gameplaymod.hardersurvival.Module_Survival;
import com.mcfht.gameplaymod.levelsnstats.Module_Levels;
import com.mcfht.gameplaymod.playerdata.PlayerData;
import com.mcfht.gameplaymod.playerdata.PlayerDataPacket;
import com.mcfht.gameplaymod.playerdata.PlayerData.Stats;
import com.mcfht.gameplaymod.playerdata.PlayerDataPacket.HandlerClient;
import com.mcfht.gameplaymod.scaffolds.Module_Scaffolds;
import com.sun.corba.se.impl.ior.ByteBuffer;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.network.NetworkRegistry;


import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;


@Mod(modid = ModCore.MODID, version = ModCore.VERSION, name = ModCore.NAME)
public class ModCore
{
    public static final String MODID = "gameplaymod";
    public static final String VERSION = "WALRUS!";
    public static final String NAME = "General MC Tweaks";
    
    public static int counter = 0;
    
    public static Module_Levels levelsMod;
    public static Module_Scaffolds scaffoldMod;    
    
    public static Module_Days module_Days;
    
    private static final List<WorldDataContainer> registeredContainers = new ArrayList<WorldDataContainer>();
    
    public static SimpleNetworkWrapper network;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent e)
    {
    	ConfigHandler.handleConfigs(new Configuration(e.getSuggestedConfigurationFile()));
    	
    	if (Settings.SCAFFOLDS_ENABLED) scaffoldMod = new Module_Scaffolds();
    	
    	network = NetworkRegistry.INSTANCE.newSimpleChannel(MODID + "channel");
        //network.registerMessage(HandlerServer.class, PlayerDataPacket.class, 0, Side.SERVER);
        network.registerMessage(HandlerClient.class, PlayerDataPacket.class, 0, Side.CLIENT);

        network.registerMessage(SettingsIntegrityPacket.HandlerClient.class, SettingsIntegrityPacket.class, 1, Side.CLIENT);
        
        network.registerMessage(Levels_RespawnPacket.HandlerServer.class, Levels_RespawnPacket.class, 2, Side.SERVER);
        network.registerMessage(Levels_RespawnPacket.HandlerClient.class, Levels_RespawnPacket.class, 2, Side.CLIENT);

        network.registerMessage(TimeUpdatePacket.HandlerClient.class, TimeUpdatePacket.class, 3, Side.SERVER);
        network.registerMessage(TimeUpdatePacket.HandlerClient.class, TimeUpdatePacket.class, 3, Side.CLIENT);


        //network.registerMessage(PlayerDataPacket.Handler.class, PlayerDataPacket.class, 0, Side.CLIENT);

    	/* 
    	 * Use this to enable or disable events in this class. Flag other booleans if you want
    	 */
        
    	boolean needInstanceBus = Settings.PENALTY_MOD_ENABLED;
    	if (needInstanceBus) 
    	{
    		FMLCommonHandler.instance().bus().register(this); 
    		MinecraftForge.EVENT_BUS.register(this);
    	}
    	
    	
    	//Now enable the modules that we want
    	if (Settings.PENALTY_MOD_ENABLED) levelsMod = new Module_Levels();

    	//MinecraftForge.EVENT_BUS.register(new Module_Survival());
    	
    	module_Days = new Module_Days();
		FMLCommonHandler.instance().bus().register(module_Days); 
    	
    	
    	if (e.getSide() == Side.CLIENT) 
		{
    		//SkyRerender 
    		//MinecraftForge.EVENT_BUS.register(new SkyRerender());
    		//FMLCommonHandler.instance().bus().register(new SkyRerender()); 
    		MinecraftForge.EVENT_BUS.register(new Levels_HUD());
		}
    	
    	registerForPersistentData(new PlayerData());
    	
    	
    	
    	PlayerData.LENGTH = Stats.values().length;
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    }

	///////////////////// DATA EVENTS ///////////////////////
	
    public static void registerForPersistentData(WorldDataContainer c)
    {
    	if (registeredContainers.contains(c) == false) registeredContainers.add(c);
    	c.enabled = true;
    }
    public static void unregisterForPersistentData(WorldDataContainer c)
    {
    	registeredContainers.remove(c);
    	c.enabled = false;
    }
    public static boolean isRegisteredContainer(WorldDataContainer c)
    {
    	return registeredContainers.contains(c);
    }
    
	/**
	* The world loading event, currently only used to pull world data from the world NBT.
	* 
	* Modifying this method, and using a similar structure to my own, should make it fairly easy to use world data.
	* Really, just copy the "If penalty mod enabled" block, then change the tagname in the WorldDataObject.loadData,
	* then change it to read your data instead of reading my NBT tags into a map.
	* @param e
	*/
	@SubscribeEvent
	public void worldLoadEvent(WorldEvent.Load e)
	{
		if (!e.world.isRemote && e.world.provider.dimensionId == 0 && registeredContainers.size() > 0) 
		{
			for (WorldDataContainer c : registeredContainers)
			{
				if (c.enabled == false) continue;
				c.persistentDataHandler = (WorldData) e.world.mapStorage.loadData(WorldData.class, c.mapName);
				if (c.persistentDataHandler == null) 
				{
					System.err.println(c.getClass().getCanonicalName() + " found no data. Either this is the first time running, or there is something wrong! ");
					c.persistentDataHandler = new WorldData(c.mapName);
					e.world.mapStorage.setData(c.mapName, c.persistentDataHandler);
				}
				else c.fromNBT(c.persistentDataHandler.data);
			}
		}
	}
	/**
	 * Fires when the world saves. This event should be used to do things like store persistent world data.
	 * To use, extend WorldDataContainer and register your child class before the world loads. See Component_Levels.
	 * @param e
	 */
	@SubscribeEvent
	public void worldSaveEvent(WorldEvent.Save e)
	{
		if (!e.world.isRemote && e.world.provider.dimensionId == 0 && registeredContainers.size() > 0) 
		{
			for (WorldDataContainer c : registeredContainers)
			{
				c.persistentDataHandler.data = c.toNBT(c.persistentDataHandler.data);
				c.persistentDataHandler.markDirty();
			}
		}
	}
}



	
