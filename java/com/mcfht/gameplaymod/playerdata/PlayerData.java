package com.mcfht.gameplaymod.playerdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagByteArray;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagIntArray;

import com.mcfht.gameplaymod.Settings;
import com.mcfht.gameplaymod.WorldDataContainer;

public class PlayerData extends WorldDataContainer {

	    /** Map containing player names, linked to their player data <b>Internal use only!</b>*/
	    private static Map<String, int[]> playerMap = new HashMap<String, int[]>();
	    
	    public static final int FORMAT_VERSION = 3;
	    
	    /** The unique ID for the data of this component. <b>Internal use only!</b>*/
	    private static final String tagName = "STPplayerdata_v" + FORMAT_VERSION;
		

	    public static int LENGTH = 0;
	    
	    public static int actualPlayerExperience = 0; //Client side only
	    public static long actualPlayerDeathTime = 0; //Client side only
	    public static int actualPlayerPenalty = 0; //Client side only
	    
		////////////////// WORLD DATA STUFFS //////////////////
	   
		
		@Override
		public void fromNBT(NBTTagCompound compound) 
		{
			final Set<String> tags = compound.func_150296_c();
			/* if (compound.getInteger("formatVersion") != 0 && compound.getInteger("formatVersion") != FORMAT_VERSION)
			{
				System.err.println("Updated mod detected! Error updating saved data! Stats have been reset!");
				for (String s : tags)
				{
					compound.removeTag(s);
				}
				return;
			}*/
			for (String s : tags)
			{
				final NBTBase base = compound.getTag(s);
				if (base != null && base instanceof NBTTagIntArray) playerMap.put(s, ((NBTTagIntArray)base).func_150302_c());
			}
		}
		
		@Override
		public NBTTagCompound toNBT(NBTTagCompound compound) 
		{
			for (Entry<String, int[]> e : playerMap.entrySet()) compound.setIntArray(e.getKey(), e.getValue());
			compound.setInteger("formatVersion", FORMAT_VERSION);
			return compound;
		}
		
		/*
		 * The following section handles my custom player stat handling. IEntityExtendedProperties
		 * seems strange and inefficient, especially when dealing with numerous stats, which must all persist through player death,
		 * and also be saved into world data.
		 */
	    
		/** Enumerator of the given player stats. Makes it impossible to fuck up basically. */
		public static enum Stats
		{
			/** The current effective level of the player. */
			LEVEL("level", 0),
			/** The number of times the player has died. */
			DEATHS("deaths", 1),
			/** The food level of at death. */
			HUNGER("food level", 2),
			/** The time the player has been alive. */
			TIMER("death timer", 3, 4),
			/**Penalty level of the player. Determines the max number of hearts that a player can have. 
			 * The value is equal to the lowest negative level that the player has attained,
			 * however it will increase over time if the player survives for long periods without dying.  */
			PENALTY("penalty amount", 5),
			
			//Leave some unused slots in case I ever want to add more features
			UNUSED1("not used, here in case I need it", 6),
			UNUSED2("not used, here in case I need it", 7);
			
			private final String ID;
			private final int index, index1;
			Stats(String name, int ind){ID = name; index = ind; index1 = 0;};
			Stats(String name, int ind, int ind1){ID = name; index = ind; index1 = ind1;};
			public String toString() { return ID; }
		}
		
	    /**
	     * The constructor is called from PreInit
	     */
		public PlayerData()
		{
			super(tagName);
			int pos = 0;
			LENGTH = Stats.values().length;
		}

		static int loadedData[];
		
		/** Gets stat from provided data object*/
		public static int getStat(Stats stat, int[] data){ 
			return data[stat.index]; 
		}
		/** Gets LONG datatype stat from provided data object*/
		public static long getStat(Stats stat, int[] data, boolean isLong){ 
			return (long)(data[stat.index]) + ((long) (data[stat.index1]) << 31L); 
		}
		
		/** Sets stat in provided data object*/
		public static void setStat(Stats stat, int[] data, int newVal) { data[stat.index] = newVal;	}
		
		public static void setStat(Stats stat, int[] data, long newVal) { 
			data[stat.index] = (int)(newVal & 0x7FFFFFFFL); 
			data[stat.index1] = (int)(newVal >> 31L);
		}
		
		/** Gets stat from currently loaded data */
		public static int getStat(Stats stat){ return loadedData == null ? 0 : loadedData[stat.index]; }
		
		/** Sets stat in currently loaded data */
		public static void setStat(Stats stat, int newVal) 
		{
			if (loadedData == null) loadedData = new int[LENGTH]; 
			loadedData[stat.index] = newVal;	
		}
		
		/** Loads the stat, increments it by one, and then stores it again. */
		public static void incrementStatAndSave(String playerName, Stats stat, int amount)
		{
			int[] val = playerMap.get(playerName);
			if ( val == null) val = new int[LENGTH];
			setStat(stat, val, getStat(stat, val) + amount);
			playerMap.put(playerName, val);
		}
		/** Loads the stat object for the player. Must call saveStats method to store any changes. */
		public static int[] loadStats(String playerName)
		{
			int[] val = playerMap.get(playerName);
			
			if (val == null || val.length < LENGTH)
			{
				setInitialStats(val = new int[LENGTH]);
				playerMap.put(playerName, val);
			}
			loadedData = val;
			return val;
		}
		
		/** Saves the provided data object for the player. */
		public static void saveStats(String playerName, int[] data)
		{
			playerMap.put(playerName, data);
		}
		
		/** Saves the currently loaded data object for the specified player. */
		public static void saveStats(String playerName)
		{
			if (loadedData != null)
			{
				playerMap.put(playerName, loadedData); 
				loadedData = null;
			}
		}
		
		public static void setInitialStats(int[] data)
		{
			setStat(Stats.PENALTY, data, Settings.HP_BASE_LEVEL + ((Settings.HP_BASE_MAX - Settings.HP_BASE_AMOUNT) * Settings.HP_BASE_STEP));
		}
		
}


