package com.mcfht.gameplaymod.playerdata;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class PlayerDataPacket  implements IMessage
{
    
    private int playerLevel;
    private long deathTime;
    private int penalty;

    public PlayerDataPacket() { }

    public PlayerDataPacket(int level, long timer, int penalty)
    {
        this.playerLevel = level;
        this.deathTime = timer;
        this.penalty = penalty;
        //System.out.println("Packet constructed at " + System.currentTimeMillis()/1000 + "by " + Thread.currentThread().getStackTrace()[2].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName() + ", ln " + Thread.currentThread().getStackTrace()[2].getLineNumber());
    }

    @Override
    public void fromBytes(ByteBuf buf) 
    {
        playerLevel =  buf.readInt();
        deathTime = buf.readLong();
        penalty = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) 
    {
        //buf.capacity(4);
        buf.writeInt(playerLevel);
        buf.writeLong(deathTime);
        buf.writeInt(penalty);
    }

    public static class HandlerClient implements IMessageHandler<PlayerDataPacket, IMessage> 
    {
        
        @Override
        public IMessage onMessage(PlayerDataPacket message, MessageContext ctx) 
        {
            PlayerData.actualPlayerExperience = message.playerLevel;
            PlayerData.actualPlayerDeathTime = message.deathTime;
            PlayerData.actualPlayerPenalty = message.penalty;
            return null;
        }
    }
}


