package com.mcfht.gameplaymod.gui.hud;

import com.mcfht.gameplaymod.Settings;
import com.mcfht.gameplaymod.Util;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.world.World;
import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class Levels_RespawnPacket implements IMessage
{
	String player;
	double[] coords = new double[3];
	
	static String[] cowardsMessages = {
		" whited out.",
		" escaped to the nearest Pok�mon Center!",
		" ran out of usable Pok�mon.",
	};
		
	
	public Levels_RespawnPacket() { }
    public Levels_RespawnPacket(String p) { player = p;}

    @Override
    public void fromBytes(ByteBuf buf) 
    {
    	if (Settings.doSpecialRespawn == false) return;
    	boolean done = false;
    	
    	
    	
    	player = new String(buf.array());
    	if (player.length() <= 1) return;
    	player = player.substring(1);	
    	
    	String message;
    	if (Util.rand.nextInt(15) == 0) message = player + cowardsMessages[1];
    	else if (Util.rand.nextInt(30) == 0) message = player + cowardsMessages[2];
    	else message = player + cowardsMessages[0];
    	
    	for (World w : MinecraftServer.getServer().worldServers)
    	{
    		for (Object p : w.playerEntities)
    		{
    			if (p instanceof EntityPlayerMP)
    			{
    				EntityPlayerMP player1 = (EntityPlayerMP) p;
    				player1.addChatComponentMessage(new ChatComponentText(message));
    				if (player1.getDisplayName().equals(player) == false) continue;
    				player1.setWorld(MinecraftServer.getServer().worldServerForDimension(Settings.COORD_RESPAWN_DIMENSION));
    	    		player1.setPositionAndUpdate(Settings.COORD_RESPAWN_X, Settings.COORD_RESPAWN_Y, Settings.COORD_RESPAWN_Z);
    			}
    		}
    	}
	
    }

    @Override
    public void toBytes(ByteBuf buf) 
    {
    	buf.writeBytes(player.getBytes());
    }

    public static class HandlerServer implements IMessageHandler<Levels_RespawnPacket, IMessage> 
    {
        @Override
        public IMessage onMessage(Levels_RespawnPacket message, MessageContext ctx)
        {
        	
        	System.out.println(message.player + cowardsMessages[Util.rand.nextInt(cowardsMessages.length)]);
            return null;
        }
    }
    
    public static class HandlerClient implements IMessageHandler<Levels_RespawnPacket, IMessage> 
    {
        @Override
        public IMessage onMessage(Levels_RespawnPacket message, MessageContext ctx)
        {
        	
            return null;
        }
    }
}


