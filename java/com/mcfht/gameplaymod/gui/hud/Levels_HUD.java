package com.mcfht.gameplaymod.gui.hud;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.mcfht.gameplaymod.ModCore;
import com.mcfht.gameplaymod.Settings;
import com.mcfht.gameplaymod.asm.Days_ASMTransformer;
import com.mcfht.gameplaymod.levelsnstats.Module_Levels;
import com.mcfht.gameplaymod.playerdata.PlayerData;
import com.mcfht.gameplaymod.playerdata.PlayerData.Stats;
import com.sun.media.sound.Toolkit;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiGameOver;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;

import javax.swing.*;

import org.lwjgl.input.Mouse;

import sun.awt.resources.awt;

import java.awt.event.*;

public class Levels_HUD extends Gui{

	
	 public static final ResourceLocation micons = new ResourceLocation(ModCore.MODID, "textures/gui/mgui.png");

	 boolean hasClicked = false;
	 boolean isTransition = false;
	 
	 String respawnLabel = "Emergency Respawn";
	 String respawnWarning = "Respawn immediately at a safe location (server specific)";
	 String respawnCoords = "Current Location: " + Settings.COORD_RESPAWN_X + ", " + Settings.COORD_RESPAWN_Y + ", " + Settings.COORD_RESPAWN_Z;
	 
	 int mX = 0;
	 int mY = 0;
	 

	 public void onRenderDeathScreen1(net.minecraftforge.client.event.GuiScreenEvent.ActionPerformedEvent e)
	 {
		 
	 }
	 
	 
	 @SubscribeEvent
	 public void onRenderDeathScreen(net.minecraftforge.client.event.GuiOpenEvent e)
	 {
		 if (Settings.doSpecialRespawn == false) return;

		 if (e.gui instanceof GuiGameOver)
		 {
			 hasClicked = false;
		 }
	 }
	 
	 @SubscribeEvent
	 public void onRenderDeathScreen(net.minecraftforge.client.event.GuiScreenEvent e)
	 {
		 if (Settings.doSpecialRespawn == false) return;

		 if (e.gui instanceof GuiGameOver)
		 {
			FontRenderer fontrenderer = Minecraft.getMinecraft().fontRenderer;
			boolean mouseOver = false;
			
			int x0 = (e.gui.width >> 1) - 100;
			int y0 = (e.gui.height >> 2) + 120;
			
	        int mX = Mouse.getEventX() * e.gui.width / Minecraft.getMinecraft().displayWidth;
	        int mY = e.gui.height - Mouse.getEventY() * e.gui.height / Minecraft.getMinecraft().displayHeight - 1;

			if (mX > x0 && mX < x0 + 200 && mY > y0 && mY < y0 + 20)
			{
				mouseOver = true;
			}
			
			if (Mouse.getEventButton() == 0)
			{
				if (mouseOver && Mouse.getEventButtonState() == true && hasClicked == false)
				{
					Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.func_147674_a(new ResourceLocation("gui.button.press"), 1.0F));
					Minecraft.getMinecraft().thePlayer.respawnPlayer();
					ModCore.network.sendToServer(new Levels_RespawnPacket(Minecraft.getMinecraft().thePlayer.getDisplayName()));
				}
				hasClicked = Mouse.getEventButtonState();
			}
			
			Minecraft.getMinecraft().getTextureManager().bindTexture(micons);
			drawString(fontrenderer, "", 0, 0, 0xFFFFFF);
			drawTexturedModalRect(x0, y0, 0, 64 + (mouseOver ? 20 : 0), 200, 20);
			
			fontrenderer.drawString(respawnLabel, x0 + 101 - (fontrenderer.getStringWidth(respawnLabel) >> 1), y0 + 6, 0x000000);
			fontrenderer.drawString(respawnLabel, x0 + 99 - (fontrenderer.getStringWidth(respawnLabel) >> 1), y0 + 6, 0x000000);
			fontrenderer.drawString(respawnLabel, x0 + 100 - (fontrenderer.getStringWidth(respawnLabel) >> 1), y0 + 7, 0x000000);
			fontrenderer.drawString(respawnLabel, x0 + 100 - (fontrenderer.getStringWidth(respawnLabel) >> 1), y0 + 5, 0x000000);
			fontrenderer.drawString(respawnLabel, x0 + 100 - (fontrenderer.getStringWidth(respawnLabel) >> 1), y0 + 6, mouseOver ? 0xEE0510 : 0xAA1010);
			
			if (mouseOver) 
			{
				drawString(fontrenderer, respawnWarning, x0 + 100 - (fontrenderer.getStringWidth(respawnWarning) >> 1), y0 + 26, 0xFFFFFF);
				drawString(fontrenderer, respawnCoords, x0 + 100 - (fontrenderer.getStringWidth(respawnCoords) >> 1), y0 + 40, 0xFFFFFF);
			}
		 }
		 Days_ASMTransformer r = new Days_ASMTransformer();
		 //r.renderSky(1.0F);
	 }

	 
	@SubscribeEvent
    public void onRenderExperienceBar(RenderGameOverlayEvent e)
	{
		if(e.isCancelable() || e.type != ElementType.EXPERIENCE || Minecraft.getMinecraft().playerController.gameIsSurvivalOrAdventure() == false)
		{      
			return;
		}
		int k = e.resolution.getScaledWidth();
	    int l = e.resolution.getScaledHeight();
		
		FontRenderer fontrenderer = Minecraft.getMinecraft().fontRenderer;
		fontrenderer.drawString(String.valueOf(Module_Levels.realWorldTime), 10, 10, 0xFFFFFF);
		if 	(Settings.GUI_SHOW_HUD_TWEAKS)
		{
			Minecraft.getMinecraft().getTextureManager().bindTexture(micons);

			int bonusHearts = Module_Levels.calcBonusHearts(PlayerData.actualPlayerExperience);
			for (int i = 0; Settings.GUI_SHOW_BONUS_HEART_OUTLINES && i < bonusHearts; i++) drawTexturedModalRect((k >> 1) - 90 + (i * 8) - (i == 0 ? 1 : 0), l - 39, i == 0 ? 0 : 1, 0, i == 0 ? 10 : 9, 10);
			
			//Now draw the faint heart outlines on the screen for missing hearts
			if (Settings.REQUIRE_TIME_FOR_REGAIN)
			{
				long ticksPassed = Minecraft.getMinecraft().theWorld.getTotalWorldTime() - PlayerData.actualPlayerDeathTime;
				int recovertTimer = Settings.HP_REGAIN > 0 ? (int) ((float) ticksPassed / (24000F / Settings.HP_REGAIN)) : 0;

				int maxExpHearts = Module_Levels.calcHearts(PlayerData.actualPlayerExperience);
				int maxTimeHearts = Module_Levels.calcHearts(PlayerData.actualPlayerPenalty) + recovertTimer;
				int currentHearts = Math.min(maxExpHearts, maxTimeHearts);
				
				if (maxTimeHearts != maxExpHearts && Settings.GUI_SHOW_MISSING_HEART_OUTLINES)
				{
					int toDraw = Math.min(Settings.HP_BASE_MAX - currentHearts, Math.max(maxTimeHearts, maxExpHearts) - currentHearts);
					for (int i = 0; i < toDraw; i++)
					{
						int heartRow = ((currentHearts + bonusHearts + i)/10);
						int dX = -91 + (((currentHearts + bonusHearts + i) * 8) % 80);
						if (dX == -91) dX -= 1;
						drawTexturedModalRect((k >> 1) + dX, l - 39 - (((currentHearts + bonusHearts + i)/10)*10), Math.min(20, Math.max(0,  10*(toDraw - 1 - i))) - (dX == -92 ? 1 : 0), 40, dX == -92 ? 10 : 9, 9);
					}
				}
				
				//Now draw the slider to show how long until hearts come back
				if (Settings.HP_REGAIN > 0 && maxTimeHearts < Settings.HP_BASE_MAX)
				{
					int t0 = (int) (24000F / Settings.HP_REGAIN);
					int sliderX = (int)(  180F * ( (float)(ticksPassed % t0) / (float)t0 )  ) - 93;
										
					//Draw the notch on the experience bar
					drawTexturedModalRect((k >> 1) - 93, l - 30, 16, 32, 2, 8);
					drawTexturedModalRect((k >> 1) + 91, l - 30, 22, 32, 2, 8);
					drawTexturedModalRect((k >> 1) + sliderX, l - 30, 0, 32, 8, 8);
				}
			}

		}
		
		//Redraw the player's experience level because... we can...
		//Actually, we have to draw the negative levels full stop, and we have to redraw the postive levels
		//so that they sit on top of the slider on the experience bar
		
		String s3 = String.valueOf(PlayerData.actualPlayerExperience);
		int j2 = (k - fontrenderer.getStringWidth(s3)) >>  1;
		int k2 = l - 31 - 4;
		
		fontrenderer.drawString(s3, j2 + 1, k2, 0);
		fontrenderer.drawString(s3, j2 - 1, k2, 0);
		fontrenderer.drawString(s3, j2, k2 + 1, 0);
		fontrenderer.drawString(s3, j2, k2 - 1, 0);
		fontrenderer.drawString(s3, j2, k2, PlayerData.actualPlayerExperience < 0 ? 0xFF0000 : 0x80FF20);
	}
	
	
}
