package com.mcfht.gameplaymod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import scala.actors.threadpool.Arrays;

import com.mcfht.gameplaymod.scaffolds.BlockScaffold;
import com.mcfht.gameplaymod.scaffolds.Module_Scaffolds;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.BlockFence;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockWall;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;
import net.minecraftforge.oredict.ShapedOreRecipe;


public class Util 
{

	public static Random rand = new Random();
	
	public static Map<Object, Float> materialStability;
	public static Map<Object, Boolean> materialJoins;

	public static int lookupCenterXZ;
	public static final float undermineThreshold = -4F;
	public static final float supportThreshold = 2F;
	public static float[][][] distLookup;
	/**
	 * Initializes the simple block physics on scaffolds
	 */
	public static void initScaffoldPhysicsStuffs()
	{
		
		
		
		materialStability = new HashMap<Object, Float>();
		
		//Supports
		materialStability.put(Blocks.bedrock, 100F);
		
		materialStability.put(BlockScaffold.class, 20F);
		materialStability.put(Material.rock, 15F);
		materialStability.put(Material.iron, 15F);
		materialStability.put(Material.wood, 12F);
		
		materialStability.put(BlockFence.class, 15F);
		materialStability.put(BlockWall.class, 13F);
		
		//underminers
		materialStability.put(Material.snow, -12F);
		
		//other
		materialStability.put(Material.grass, 2F);
		materialStability.put(Material.ground, 0.5F);
		materialStability.put(Material.cloth, -8F);
		materialStability.put(Material.plants, -18F);
		materialStability.put(Material.craftedSnow, -12F);
		materialStability.put(Material.sand, -6F);
		materialStability.put(BlockFalling.class, -18F);
		
		materialJoins = new HashMap<Object, Boolean>();
		
		materialJoins.put(Material.rock, true);
		materialJoins.put(Material.iron, true);
		materialJoins.put(Material.wood, true);
		//materialJoins.put(Material.grass, true);
		
		
		//Create a lookup table
		int maxWeight = 0;
		for (int i : Module_Scaffolds.weights)
		{
			maxWeight = Math.max(maxWeight, i);
		}
		
		lookupCenterXZ = (maxWeight/3) + 2;
		distLookup = new float[maxWeight * 2][(lookupCenterXZ << 1) + 1][(lookupCenterXZ << 1) + 1];
		for (int i = 0; i < distLookup.length; i++)
			for (int j = 0; j < distLookup[i].length; j++)
				for (int k = 0; k < distLookup[i][j].length; k++)
					distLookup[i][j][k] = (float) Math.sqrt((i * i) + ((j - lookupCenterXZ) * (j - lookupCenterXZ)) + (( k - lookupCenterXZ) * (k - lookupCenterXZ)));
		
		
	}
	
	public static float getBlockStability(Block b)
	{
		Float f = materialStability.get(b);
		if (f == null) f = materialStability.get(b.getClass());
		if (f == null) f = materialStability.get(b.getClass().getSuperclass());
		if (f == null) f = materialStability.get(b.getMaterial());
		if (f == null) return 0F;
		return f.floatValue();
	}
	public static boolean doesBlockJoin(Block b)
	{
		Boolean f = materialJoins.get(b);
		if (f == null) f = materialJoins.get(b.getMaterial());
		return (f != null);
	}
	//////////RANDOM METHODS /////////
	
	/**
	 * Calculates a random variance from the given value
	 * @param baseValue
	 * @param maxDeviation
	 * @param rand
	 * @return 
	 */
    public static final float randomize(float baseValue, float maxDeviation)
    {
    	return (baseValue - maxDeviation) + (2 * maxDeviation * rand.nextFloat());
    }
    
    
    public static int intDir[][] = {{0,-1,0}, {0, 1, 0}, {0, 0, -1},{0, 0, 1},{1, 0, 0},{-1, 0, 0}};
    
    public static class BlockPos
    {
    	public int[] pos;
    	public BlockPos()
    	{
    	 pos = new int[3];	
    	}
    	public BlockPos(int... xyz)
    	{
    		this();
    		for (int i = 0; i < Math.min(xyz.length, 3); i++) pos[i] = xyz[i];
    	}
    	public static BlockPos dotProd(BlockPos pos0, BlockPos pos1)
    	{
    		return new BlockPos(pos0.pos[0]*pos1.pos[0],pos0.pos[1]*pos1.pos[1],pos0.pos[2]*pos1.pos[2]);
    	}
    	/** Dot Product between 2 xyz cordinates (x0,y0,z0,x1,y1,z1)*/
    	public static BlockPos dotProd(int... pos)
    	{
    		return new BlockPos(pos[0] * pos[3], pos[1] * pos[4], pos[2] * pos[5]);
    	}
    	/** Move the position towards the face designated by the integer (follows conventional MC face numbers) */
    	public BlockPos offset(int dir)
    	{
    		return new BlockPos(pos[0] + intDir[dir % 6][0], pos[1] + intDir[dir % 6][1], pos[2] + intDir[dir % 6][2]);
    	}
    	public BlockPos subtract(BlockPos pos1)
    	{
    		return new BlockPos(pos[0] - pos1.pos[0], pos[1] - pos1.pos[1], pos[2] - pos1.pos[2]);
    	}
    	public BlockPos add(BlockPos pos1)
    	{
    		return new BlockPos(pos[0] + pos1.pos[0], pos[1] + pos1.pos[1], pos[2] + pos1.pos[2]);
    	}
    	public BlockPos dotProd(BlockPos pos1)
    	{
    		return new BlockPos(pos[0]*pos1.pos[0], pos[1]*pos1.pos[1], pos[2]*pos1.pos[2]);
    	}
    	public float[] dotProd(float... pos1)
    	{
    		return new float[]{
				((float)pos[0])*pos1[0],
				((float)pos[1])*pos1[1],
				((float)pos[2])*pos1[2]
    		};
    	}
    	/** Normalizes the vector from this position to the given position as float*/
    	public float[] normalizeDir(BlockPos pos1)
    	{
    		BlockPos posN = this.subtract(pos1);
    		float dist = 1F/(float)Math.sqrt((float)posN.dotProd(posN).sum());
    		return posN.dotProd(dist, dist, dist);
    	}
    	/** returns the 6 directions in order of optimal-ness...*/
    	public int[] getOptimalDirs(BlockPos pos1)
    	{
    		float[] dirs = normalizeDir(pos1), _dirs = new float[3];
    		System.arraycopy(dirs, 0, _dirs, 0, 3);
    		Arrays.sort(dirs);
    		float[] best = new float[3]; int[] index = new int[3];
    		for (int i = 0; i < 3; i++)
    		{
    			if (_dirs[i] == dirs[0]) { best[i] = dirs[0]; index[i] = 0; }
    			else if (_dirs[i] == dirs[1]) { best[i] = dirs[1]; index[i] = 1; }
    			else { best[i] = dirs[2]; index[i] = 2; }
    		}
    		
			int[] result = new int[6];
			result[0] = (index[0] << 1) + best[0] < 0 ? 0 : 1;
			result[1] = (index[1] << 1) + best[1] < 0 ? 0 : 1;
			result[2] = (index[2] << 1) + best[2] < 0 ? 0 : 1;
			result[3] = result[2] ^ 0x1;
			result[4] = result[1] ^ 0x1;
			result[5] = result[0] ^ 0x1;
    		
			return result;
    		
    	}
    	public int sum()
    	{
    		return pos[0] + pos[1] + pos[2];
    	}
    	public boolean isNegative()
    	{
    		return pos[0] < 0 || pos[1] < 0 || pos[2] < 0;
    	}
    	@Override
    	public String toString()
    	{
    		return "[" + pos[0] + ", " + pos[1] + ", " + pos[2] + "]";
    	}
    	
    	@Override
    	public int hashCode()
    	{
			return (pos[0] << 10) | (pos[1] << 10) | (pos[2]);
    	}
    	
    	/** Faster getblock call. */
    	public Block getBlock(World w)
    	{
    		return w.getChunkFromChunkCoords(pos[0] >> 4, pos[2] >> 4).getBlock(pos[0] & 0xF, pos[1], pos[2] & 0xF);
    		
    		/*
    		return w.getChunkFromChunkCoords(pos[0] >> 4, pos[2] >> 4)
    				.getBlockStorageArray()[pos[1] >> 4]
    				.getBlockByExtId(pos[0] & 0xFF, pos[1] & 0xFF, pos[2] & 0xFF);*/
    	}
    	/** Faster getmetadata call */
    	public int getBlockMetadata(World w)
    	{
    		return w.getChunkFromChunkCoords(pos[0] >> 4, pos[2] >> 4).getBlockMetadata(pos[0] & 0xF, pos[1], pos[2] & 0xF);
    		/*
    		return w.getChunkFromChunkCoords(pos[0] >> 4, pos[2] >> 4)
    				.getBlockStorageArray()[pos[1] >> 4]
    				.getExtBlockMetadata(pos[0] & 0xFF, pos[1] & 0xFF, pos[2] & 0xFF);*/
    	}
    }
   
    /**
     * Attempts to estimate the stability of the terrain at this location.
     * 
     * @param w
     * @param x0
     * @param y0
     * @param z0
     * @param depth
     * @param maxRadius - Depth at which we cease spanning out horizontally. Area of subsequent layers will be (2n-1)^2
     * @return
     */
    public static float testGoundStability(World w, Block b0, int x0, int y0, int z0, int depth, int maxRadius)
    {
    	if (w.isRemote == true) return 1F;
    	float groundStability = 0;
		/* We travel down through the connected blocks below us, in a rectangle area.
		 *
		 * Essentially, the algorithm should just walk to each connected block, flag it,
		 * and then continue on.
		 */
    	
    	Block[][][] flagged = new Block[distLookup.length][distLookup[0].length][distLookup[0].length];
		
    	/*
		for (int i = 0; i < flagged.length; i++)
			for (int j = 0; j < flagged[0].length; j++)
				for (int k = 0; k < flagged[0].length; k++)
					flagged[i][j][k] = null;
		*/
		
		BlockPos pos0 = new BlockPos(x0, y0, z0);
		
		//Set the upper and lower bounds for block map
		BlockPos posGridOrigin = pos0.subtract(new BlockPos(distLookup[0].length >> 1, (distLookup.length - 1), distLookup[0].length >> 1));
		BlockPos posGridBound = pos0.add(new BlockPos(distLookup[0].length >> 1, 0, distLookup[0].length >> 1));
		
		BlockPos posM = pos0.subtract(posGridOrigin);
		flagged[posM.pos[1]][posM.pos[0]][posM.pos[2]] = b0;
		
		System.out.println("Starting Walk between " + posGridOrigin.toString() + " and " + posGridBound.toString());
		int found = doRecursiveWalk(w, pos0, flagged, posGridOrigin, posGridBound, 0);
		
		for (int i = 0; i < flagged.length; i++)
			for (int j = -Math.min(i, maxRadius); j <= Math.min(i, maxRadius); j++)
				for (int k = Math.min(i, maxRadius); k <= Math.min(i, maxRadius); k++)
				{
					Block bN = flagged[i][j + maxRadius][k + maxRadius];
					if (bN == null || bN == Blocks.air || bN instanceof BlockLiquid) continue; 
					float value = getBlockStability(flagged[i][j + maxRadius][k + maxRadius]);
					float dist = distLookup[i][j + maxRadius][k + maxRadius];
					//System.out.println("   :   [" + i + ", " + j + ", " + k + "], block strength: " + value + ", dist: " + dist);
				}
		
		System.out.println("Number of blocks found: " + found);
		
		return groundStability <= 0F ? groundStability : (float)Math.sqrt(groundStability);
		
    }
    
    /** Recurse through blocks until all the connected blocks are flagged. Uses a fixed size array.
     *  Larger scale implementation should use directional biases (see: BlockPos.normalizeDir), regional arrays, and stage calcs over
     * 	multiple ticks.
     *  
     * @param w
     * @param pos0
     * @param flagged
     * @param posGridOrigin
     * @return
     */
    public static int doRecursiveWalk(World w, BlockPos pos0, Block[][][] flagged, BlockPos posGridOrigin, BlockPos posGridBound, int stabilityChance)
    {
    	int result = 0;
    	BlockPos posN;
    	for (int i = 0; i < 6; i++)
    	{
    		posN = pos0.offset(i);
    		BlockPos posM = posN.subtract(posGridOrigin);
    		if (posM.isNegative() || posGridBound.subtract(posN).isNegative()) continue;
    		if (flagged  [posM.pos[1]]  [posM.pos[0]]  [posM.pos[2]] == null)
    		{
    			Block bN = w.getBlock(posN.pos[0], posN.pos[1], posN.pos[2]);
    			flagged[posM.pos[1]][posM.pos[0]][posM.pos[2]] = bN;
    			result += 1;
    			//If the blocks can be joined, (or if randomly joining is possible?)
    			if ( doesBlockJoin(bN)/* || (stabilityChance > 0 && bN != Blocks.air && rand.nextInt(stabilityChance) == 0)*/) 
    				result += doRecursiveWalk(w, posN, flagged, posGridOrigin, posGridBound, stabilityChance);
    		}
    	}
    	return result;
    }
    
    
   public static void removeRecipes(List<Object> items)
   {
	   Iterator toRemoveIt = items.iterator();
	   
	   while (toRemoveIt.hasNext())
	   {
		   Object toRemove = toRemoveIt.next();
		   
		   if (toRemove instanceof Item) removeItemRecipe((Item)toRemove);
		   else if (toRemove instanceof Block) removeBlockRecipe((Block)toRemove);
		   
		   toRemoveIt.remove();
	   }
	   
	   List l = CraftingManager.getInstance().getRecipeList();
	  
   }
   
   public static void removeItemRecipe(Item i)
   {
	   Iterator it = CraftingManager.getInstance().getRecipeList().iterator();
	   while (it.hasNext())
	   {
		   Object o = it.next();
		   if (o instanceof ShapedRecipes && ((ShapedRecipes)o).getRecipeOutput().getItem() == i)
			   it.remove();
		   else if (o instanceof ShapelessRecipes && ((ShapelessRecipes)o).getRecipeOutput().getItem() == i)
			   it.remove();
		   else if (o instanceof ShapedOreRecipe && ((ShapedOreRecipe)o).getRecipeOutput().getItem() == i)
			   it.remove();
	   }
   }
   
   public static void removeBlockRecipe(Block i)
   {
	   Iterator it = CraftingManager.getInstance().getRecipeList().iterator();
	   while (it.hasNext())
	   {
		   Object o = it.next();
		   if (o instanceof ShapedRecipes && ((ShapedRecipes)o).getRecipeOutput().getItem() == Item.getItemFromBlock(i))
			   it.remove();
		   else if (o instanceof ShapelessRecipes && ((ShapelessRecipes)o).getRecipeOutput().getItem() == Item.getItemFromBlock(i))
			   it.remove();
		   else if (o instanceof ShapedOreRecipe && ((ShapedOreRecipe)o).getRecipeOutput().getItem() == Item.getItemFromBlock(i))
			   it.remove();
	   }
   }
   
}
