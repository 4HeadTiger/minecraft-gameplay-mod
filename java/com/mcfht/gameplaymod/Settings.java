package com.mcfht.gameplaymod;

/**
 * Contains various global settings for the mod
 * @author FHT
 *
 */
public class Settings {

	///////////////// MODULES //////////////////
	
    public static boolean PENALTY_MOD_ENABLED = true;
    public static boolean SCAFFOLDS_ENABLED = true;
	
	//////////// HEALTH SETTINGS ////////////
    /** Starting HP. Note: each heart is worth 2. */
    public static int HP_BASE_AMOUNT = 3 << 1;
    /** Amount of HP to gain per step. Note: each heart is worth 2.*/
    public static int HP_BASE_GAIN = 1 << 1;
    /** The number of levels between each step. */
    public static int HP_BASE_STEP = 5;
    /** The max number of hearts that can be had. Note: each heart is worth 2. */
    public static int HP_BASE_MAX = 10 << 1;
    /** The initial level at which the player will have the minimum number of hearts */
    public static int HP_BASE_LEVEL = -30;
    public static int HP_INTERNAL_BASE_LEVEL_MULTIPLIER;

    /** The number of hearts that can be "regained" per day */
    public static float HP_REGAIN = 1;
   
    /** Internal base multiplier number for survival timer */
    public static final int timerBaseHeartScale = 7200;
    /** Internally calculated multiplier for heart regain rate */
    public static int timerHeartScale = (int) ((float)timerBaseHeartScale/HP_REGAIN);
    
	//////////// HEALTH SETTINGS ////////////
    /** Amount of HP to gain per step. Note: each heart is worth 2. */
    public static int HP_BONUS_GAIN;
    /** The number of bonus levels between each step. */
    public static int HP_BONUS_STEP;
    /** The max number of bonus hearts that can be had. Note: each heart is worth 2. */
    public static int HP_BONUS_MAX;
    /** The starting level for bonus hearts */
    public static int HP_BONUS_LEVEL;
    
    ////////////// HUNGER SETTINGS //////////////////
    
    public static int HUNGER_MOD;
    public static int HUNGER_MIN;
    
    ////////////// LEVEL SETTINGS //////////////////
    

    /** Internal setting, whether or not the player will drop XP */
    public static boolean dropXP = true;
    
    
    /** Internal setting, whether or not the player will retain XP */
    public static boolean LEVELS_DO_FANCY = true;
    
    public static int LEVEL_LOSS_NEGATIVE = 10;
    public static int LEVEL_LOSS_POSITIVE_MULT = 3;
    public static int LEVEL_LOSS_NEGATIVE_DIMINISH_FACTOR = 6;
    public static int LEVEL_MINIMUM = -100;
    
    /** Seconds between Night Time XP Gains */
    public static int XP_NIGHT_RATE_POSITIVE = 15;
    /** Seconds between Night Time XP Gains for negative level players */
    public static int XP_NIGHT_RATE_NEGATIVE = 10;
    
    //////////////// GUI SETTINGS //////////////////
    
    public static boolean GUI_SHOW_HUD_TWEAKS = true;
    public static boolean GUI_SHOW_BONUS_HEART_OUTLINES = true;
    public static boolean GUI_SHOW_MISSING_HEART_OUTLINES = true;
    
    public static boolean REQUIRE_TIME_FOR_REGAIN = true;

    
    /** XP lost at respawn (base value) */
    public static float XP_LOSE_BASE = 3F;
    /** Max deviation from this multipler (recalculated at config read) */
    public static float XP_LOSE_VAR = 0.5F;
    /** XP to drop multipler*/
    public static float XP_DROP_BASE = 3F;
    /** XP to drop variance */
    public static float XP_DROP_VAR = 0.5F;
    /** Whether to penalize the player's hunger on death/respawn */
    public static boolean doHungerPenalty = true;
    
    /////////////////// SCAFFOLDS SETTINGS ////////////////////////
    
    /** Whether scaffolds will test their terrain for suitability */
	public static boolean enableTerrainTesting = true;
	
    
    //////////////////////// HARDER SURVIVAL SETTINGS ///////////////////
	
	public static boolean HARDER_BEDS = true;
	
	
	///////////////////// SPECIAL DEATH LOCATION SETTINGS ///////////////////////
	public static int COORD_RESPAWN_DIMENSION = 0;
	public static int COORD_RESPAWN_X = 0;
	public static int COORD_RESPAWN_Y = 0;
	public static int COORD_RESPAWN_Z = 0;
	public static boolean doSpecialRespawn = false;
	public static int MAX_RANDOM_OFFSET = 0;
	
	//////////////////////////// DAY LENGTH MODIFIER ///////////////////////////
	public static double TIME_SCALE = 50D;
	public static double TIME_DAY_RATIO = 0.5D; //Amount of the day that is daytime
	public static double TIME_DAY_MAX_STRETCH = 0.1D; //How much the day ratio can stretch
	
	public static int STAR_DENSITY = 2000; //Density of stars. Vanilla is 1500.
	
	
	public static double dayShiftFactor = 0D; //Shifts the light/dark cycle around by a small amount
	public static double dayShiftMax = 0.1D; //How much the day/night cycle can be shifted
	
	public static double dayTimeCounter = 0D;
	public static int daysPassed = 0;
	
}
