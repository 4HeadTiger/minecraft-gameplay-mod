package com.mcfht.gameplaymod.asm;

import java.util.Map;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;

public class ASMLoadingPlugin implements IFMLLoadingPlugin
{

	@Override
	public String[] getASMTransformerClass() {
		System.out.println("######################## LOADING PLUGIN ###########################");
		return new String[]{Days_ASMTransformer.class.getName()};
	}

	@Override
	public String getModContainerClass() {
		return null;
	}

	@Override
	public String getSetupClass() {
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data) {
		
	}

	@Override
	public String getAccessTransformerClass() {
		return null;
	}


}
