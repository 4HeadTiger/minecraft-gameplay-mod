package com.mcfht.gameplaymod.asm;

import java.util.Iterator;

import org.lwjgl.opengl.GL11;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.InvokeDynamicInsnNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeAnnotationNode;
import org.objectweb.asm.tree.VarInsnNode;

import com.mcfht.gameplaymod.Settings;
import com.mcfht.gameplaymod.Util;
import com.mcfht.gameplaymod.daylength.Module_Days;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.client.IRenderHandler;

/**
 * This class overrides the sky rendering method from the vanilla renderer, and sends it to our own method instead.
 * 
 * <p>We will make two amendments. The first redirects the method call within the renderer. The second hooks into the
 * actual method, and sends it to our own, just to be safe (just in case something else calls that method)
 * @author FHT
 *
 */
public class Days_ASMTransformer implements IClassTransformer {

	public static final String packageName = "com/mcfht/gameplaymod/daylength/";
	public static final String className = Module_Days.class.getSimpleName().replace(".class", "");
	

	
	@Override
	public byte[] transform(String name, String transformedName, byte[] basicClass) 
	{
		/*
		//Redirect method calls
		if (name.contains("EntityRenderer")) return redirect_renderSky(name, basicClass, false);

		//Override the entire methods for safety
		if (name.contains("RenderGlobal")) return override_renderSky(name, basicClass, false);
		if (name.equals("net.minecraft.world.World")) return override_getCelestialAngle(name, basicClass, false);
		*/
		if (name.contains("EntityRenderer") || name.contains("RenderGlobal") || name.equals("net.minecraft.world.World"))
				return doPatching(name, basicClass, false);
		
		return basicClass;
	}

	
	public byte[] doPatching(String name, byte[] bytes, boolean obfuscated) 
	{
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(bytes);
		classReader.accept(classNode, 0);
		
		@SuppressWarnings("unchecked")
		Iterator<MethodNode> methods = classNode.methods.iterator();
		methodLoop:
		while(methods.hasNext())
		{
			MethodNode m0 = methods.next();
			if (m0.name.equals("getCelestialAngle"))
			{
				for (int i = 0; i < 4; i++) m0.instructions.remove(m0.instructions.get(3));
				Iterator<AbstractInsnNode> iter = m0.instructions.iterator();
				while (iter.hasNext())
				{
					AbstractInsnNode n = iter.next();
					if (n instanceof MethodInsnNode)
					{
						//redirect to the masking method, pass the world parameter just in case we need it for stuff
						m0.instructions.set(n, new MethodInsnNode(Opcodes.INVOKESTATIC, packageName + className, "getSolarAngleFractionMask2", "(Lnet/minecraft/world/World;F)F", false));
						break methodLoop;
					}
				}
			}
			if (m0.name.contains("renderWorld"))
			{
				Iterator<AbstractInsnNode> iter = m0.instructions.iterator();
				while (iter.hasNext())
				{
					AbstractInsnNode n = iter.next();
					if (n instanceof MethodInsnNode)
					{
						MethodInsnNode n0 = (MethodInsnNode)n;
						if (n0.name.equals("renderSky"))
						{
							//Simply redirect the method call so that it runs our own version of the method
							m0.instructions.remove(n.getPrevious().getPrevious());
							m0.instructions.set(n0, new MethodInsnNode(Opcodes.INVOKESTATIC, packageName + className, "renderSky", "(F)V", false));
							break methodLoop;
						}
					}
				}
			}
			if (m0.name.equals("renderSky"))
			{
				//Call our own method, then return early
				m0.instructions.insert(m0.instructions.get(0), new VarInsnNode(Opcodes.FLOAD, 1));
				m0.instructions.insert(m0.instructions.get(1), new MethodInsnNode(Opcodes.INVOKESTATIC, packageName + className, "renderSky", "(F)V", false));
				m0.instructions.insert(m0.instructions.get(2), new InsnNode(Opcodes.RETURN));
				break methodLoop;
			}
		}
		
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		classNode.accept(writer);
		return writer.toByteArray();
	}
	/*
	public byte[] override_getCelestialAngle(String name, byte[] bytes, boolean obfuscated) 
	{
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(bytes);
		classReader.accept(classNode, 0);
		
		@SuppressWarnings("unchecked")
		Iterator<MethodNode> methods = classNode.methods.iterator();
		methodLoop:
		while(methods.hasNext())
		{
			MethodNode m0 = methods.next();
			if (m0.name.equals("getCelestialAngle"))
			{
				for (int i = 0; i < 4; i++) m0.instructions.remove(m0.instructions.get(3));
				Iterator<AbstractInsnNode> iter = m0.instructions.iterator();
				while (iter.hasNext())
				{
					AbstractInsnNode n = iter.next();
					if (n instanceof MethodInsnNode)
					{
						//redirect to the masking method, pass the world parameter just in case we need it for stuff
						m0.instructions.set(n, new MethodInsnNode(Opcodes.INVOKESTATIC, packageName + className, "getSolarAngleFractionMask2", "(Lnet/minecraft/world/World;F)F", false));
						break methodLoop;
					}
				}
			}
		}
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		classNode.accept(writer);
		return writer.toByteArray();
	}
	
	public byte[] redirect_renderSky(String name, byte[] bytes, boolean obfuscated) 
	{
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(bytes);
		classReader.accept(classNode, 0);
		
		@SuppressWarnings("unchecked")
		Iterator<MethodNode> methods = classNode.methods.iterator();
		methodLoop:
		while(methods.hasNext())
		{
			MethodNode m0 = methods.next();
			if (m0.name.contains("renderWorld"))
			{
				Iterator<AbstractInsnNode> iter = m0.instructions.iterator();
				while (iter.hasNext())
				{
					AbstractInsnNode n = iter.next();
					if (n instanceof MethodInsnNode)
					{
						MethodInsnNode n0 = (MethodInsnNode)n;
						if (n0.name.equals("renderSky"))
						{
							//Simply redirect the method call so that it runs our own version of the method
							m0.instructions.remove(n.getPrevious().getPrevious());
							m0.instructions.set(n0, new MethodInsnNode(Opcodes.INVOKESTATIC, packageName + className, "renderSky", "(F)V", false));
							break methodLoop;
						}
					}
				}
			}
		}
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		classNode.accept(writer);
		return writer.toByteArray();
	}
	
	public byte[] override_renderSky(String name, byte[] bytes, boolean obfuscated) 
	{
		ClassNode classNode = new ClassNode();
		ClassReader classReader = new ClassReader(bytes);
		classReader.accept(classNode, 0);
		
		@SuppressWarnings("unchecked")
		Iterator<MethodNode> methods = classNode.methods.iterator();
		methodLoop:
		while(methods.hasNext())
		{
			MethodNode m0 = methods.next();
			if (m0.name.contains("renderSky"))
			{
				//Call our own method, then return early
				m0.instructions.insert(m0.instructions.get(0), new VarInsnNode(Opcodes.FLOAD, 1));
				m0.instructions.insert(m0.instructions.get(1), new MethodInsnNode(Opcodes.INVOKESTATIC, packageName + className, "renderSky", "(F)V", false));
				m0.instructions.insert(m0.instructions.get(2), new InsnNode(Opcodes.RETURN));
				break methodLoop;
			}
		}
		
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		classNode.accept(writer);
		return writer.toByteArray();
	}*/
}
