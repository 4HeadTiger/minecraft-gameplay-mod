package com.mcfht.gameplaymod.levelsnstats;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingDeathEvent;

import com.mcfht.gameplaymod.ModCore;
import com.mcfht.gameplaymod.Settings;
import com.mcfht.gameplaymod.SettingsIntegrityPacket;
import com.mcfht.gameplaymod.playerdata.PlayerData;
import com.mcfht.gameplaymod.playerdata.PlayerData.Stats;
import com.mcfht.gameplaymod.playerdata.PlayerDataPacket;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.PlayerTickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;

public class Module_Levels
{
	
	
	/** The unique ID for the data of this component. <b>Internal use only!</b>*/
    public final static String tagName = "mcfhtPenaltyComponent";
	
    
	////////////////// WORLD DATA STUFFS //////////////////
    /**
     * This is the Death Penalty component. This handles everything from player leveling, XP dropping, etc.
     * 
     * The constructor is called from PreInit. TODO: Add any code to modify Dungeon Loots here.
     */
	public Module_Levels()
	{
		//Register for events
		MinecraftForge.EVENT_BUS.register(this);
		FMLCommonHandler.instance().bus().register(this);
	}


    
	 ///////////////////// EVERYTHING BELOW THIS LINE CAN BE EDITED PRETTY SAFELY ////////////////////////	
    
    /////////////////////////EVENTS///////////////////////

	/**
	 * In this event, we make sure that the player is updated as soon as they enter the game. This allows us
	 * to enforce updates far less frequently - the game already takes care of world time between client and server,
	 * so we just have to update when player level changes.
	 */
	@SubscribeEvent
	public void PlayerEntersGame(PlayerEvent.PlayerLoggedInEvent e)
	{
		
		EntityPlayerMP player = (EntityPlayerMP) e.player;
		String name = player.getDisplayName();

		//Ensure that the player is using the server's settings (or the ones that matter anyway)
		ModCore.network.sendTo(new SettingsIntegrityPacket(), player);
		
		int[] stats = PlayerData.loadStats(name);
		int level = PlayerData.getStat(Stats.LEVEL, stats);
		int timer = PlayerData.getStat(Stats.TIMER, stats);
		int penalty = PlayerData.getStat(Stats.PENALTY, stats);
		
		//Update the timer because everything likes to shit itself for some reason
		PlayerData.setStat(Stats.TIMER, stats, Math.max(player.isDead ? 0 : 1, timer));
		ModCore.network.sendTo(new PlayerDataPacket(level, timer, penalty), player);
		
		//Calculate the player's current health
		int oldMax = (int) player.getMaxHealth();
		int newMax = calcHealth(level);
		
		//Factor in time if required
		if (Settings.REQUIRE_TIME_FOR_REGAIN)
		{
			timer = Settings.HP_REGAIN > 0 ? (int) ((float) (player.worldObj.getTotalWorldTime() - timer) / (24000F / Settings.HP_REGAIN)) : 0;
			int timeMax = calcHealth(penalty) + (int)(timer << 1);
			if (newMax > timeMax) newMax = timeMax;
		}
		
		//Add bonus hearts
		newMax += calcBonusHealth(level);
		
		if (newMax == oldMax) return;
		
		player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(newMax);
		
		//Setting the max health does not update the actual health, so we have to do that manually
		player.setHealth(newMax > oldMax ? (int) (player.getHealth() + (newMax - oldMax)) : Math.min((int) player.getHealth(), newMax));
		
	}

	/**
	 * When the player comes back alive, we have to store when they came back alive, and reset all their stats from the
	 * saved values.
	 * @param e
	 */
    @SubscribeEvent
	public void playerRespawnEvent(PlayerEvent.PlayerRespawnEvent e)
	{
		//Only run on server.
		if (e.player instanceof EntityPlayerMP == false) return; //server only
		EntityPlayerMP player = (EntityPlayerMP) e.player;
		String name = player.getDisplayName();
		
		//Read the relevant stats
		int[] stats = PlayerData.loadStats(name);
		int level = PlayerData.getStat(Stats.LEVEL, stats);
		
		//Reset timer to not 0, sledge hammer fix to updates occurring at death
		PlayerData.setStat(Stats.TIMER, stats, player.worldObj.getTotalWorldTime());
		PlayerData.saveStats(name, stats);
		
		//Now ensure integrity between player level and stored level
		player.experienceLevel = 0;
		if (level > 0) player.addExperienceLevel(Math.max(level, 0));
		
		//Send the new data to the player
		ModCore.network.sendTo(new PlayerDataPacket(level, player.worldObj.getTotalWorldTime(), PlayerData.getStat(Stats.PENALTY, stats)), player);

		//Update hunger yo
		player.getFoodStats().addStats(Math.max(PlayerData.getStat(Stats.HUNGER, stats) + (Settings.HUNGER_MOD << 1), Settings.HUNGER_MIN << 1) - 20, 20F);
		
		//Force the player health
		int newMax = Math.min(calcHealth( PlayerData.getStat(Stats.PENALTY, stats)), calcHealth(level)) + calcBonusHealth(level);
		player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(newMax);
		player.setHealth(newMax);
	}
	
    //Some random snippet for spawning experience orbs
	//player.worldObj.spawnEntityInWorld(new EntityXPOrb(player.worldObj, player.posX, player.posY, player.posZ, j));
    
    /**
     * Fires when the player dies. We have to notify the timer that the player is dead, to do this
     * we will set it to zero.
     * @param e
     */
	@SubscribeEvent
	public void playerDed(LivingDeathEvent e)
	{
		if (e.entity instanceof EntityPlayerMP == false) return; //server only
		EntityPlayerMP player = (EntityPlayerMP) e.entity;
		String name = player.getDisplayName();
		
		int[] stats = PlayerData.loadStats(name);
		int level = PlayerData.getStat(Stats.LEVEL, stats);
		int penalty = PlayerData.getStat(Stats.PENALTY, stats);
		int timer = (int) (player.worldObj.getTotalWorldTime() - PlayerData.getStat(Stats.TIMER, stats));
		
		//Force stored level to match plaer's level
		if (player.experienceLevel > 0) level = player.experienceLevel;
		
		//Decrease the player's level iff we are allowed to go negative, otherwise, player loses all experience
		if (Settings.LEVEL_MINIMUM != 0)
		{
			//Calculate where the new level will be
			if (level >= Settings.LEVEL_LOSS_NEGATIVE * Settings.LEVEL_LOSS_POSITIVE_MULT) level = player.experienceLevel -= Settings.LEVEL_LOSS_NEGATIVE * Settings.LEVEL_LOSS_POSITIVE_MULT;
			else if (level > 0) level = 0 - (((Settings.LEVEL_LOSS_NEGATIVE * Settings.LEVEL_LOSS_POSITIVE_MULT) - level)/Settings.LEVEL_LOSS_POSITIVE_MULT);
			else level -= Math.max(1, Settings.LEVEL_LOSS_NEGATIVE + (level/Settings.LEVEL_LOSS_NEGATIVE_DIMINISH_FACTOR));

			//Now clamp it
			level = Math.max(level, Settings.LEVEL_MINIMUM);
		}
		else level = 0;
		
		//update the player's actual experience level
		player.experienceLevel = Math.max(0, level);
		
		//If the penalty is to become worse, make it worse. Otherwise, improve it according to how long the player
		//has survived for (otherwise you can level up and die again to lose the penalty without waiting
		if (level <= penalty) penalty = level;
		else if (penalty < 0)
		{
			timer = Settings.HP_REGAIN > 0 ? (int) ((float) (player.worldObj.getWorldTime() - timer) / (24000F / Settings.HP_REGAIN)) : 0;
			penalty += (timer * Settings.HP_BASE_STEP);
		}
		
		//Set and save all the player's stats
		PlayerData.setStat(Stats.LEVEL, stats, level);
		PlayerData.setStat(Stats.PENALTY, stats, penalty);
		PlayerData.setStat(Stats.DEATHS, stats, PlayerData.getStat(Stats.DEATHS, stats) + 1);
		PlayerData.setStat(Stats.TIMER, stats, 0);
		PlayerData.setStat(Stats.HUNGER, stats, player.getFoodStats().getFoodLevel());
		PlayerData.saveStats(name, stats);
		
		ModCore.network.sendTo(new PlayerDataPacket(level, timer, penalty), player);
	}
	

	public static double realWorldTime = 0D;
	int counter = 0;
	/**
	 * Entity updates are not reliable. Therefore, for events that have to happen with some degree of accuracy, we
	 * should use the server tick to iterate over players (there will not be many players, so this is fairly inexpensive).
	 * @param e
	 */
	@SubscribeEvent
	public void serverTick(ServerTickEvent e)
	{
		++counter;
		if (counter % 20 == 0)
		{
			for (WorldServer w : MinecraftServer.getServer().worldServers)
			{
				for (Object _p : w.playerEntities)
				{
					//if (_p instanceof EntityPlayerMP == false) continue;
					EntityPlayerMP p = (EntityPlayerMP) _p;
					String name = p.getDisplayName();
					
					
					
					//Ensure integrity every now and then
					if (counter % 900 == 0)
					{
						int[] stats = PlayerData.loadStats(name);
						int level = PlayerData.getStat(Stats.LEVEL, stats);
						ModCore.network.sendTo(new PlayerDataPacket(PlayerData.getStat(Stats.LEVEL, stats), PlayerData.getStat(Stats.TIMER, stats, true), PlayerData.getStat(Stats.PENALTY, stats)), p);
					}
					//Now we have to give players their xp at night time
					long time = w.getWorldTime() % 24000;
					if (time > 13000 && time < 23000)
					{
						int[] stats = PlayerData.loadStats(name);
						int level = PlayerData.getStat(Stats.LEVEL, stats);
						if (level < 0 && Settings.XP_NIGHT_RATE_NEGATIVE > 0)
						{
							if ((counter/20) % Settings.XP_NIGHT_RATE_NEGATIVE == 0) p.addExperience(1);
						}
						else if (Settings.XP_NIGHT_RATE_POSITIVE > 0)
						{
							if ((counter/20) % Settings.XP_NIGHT_RATE_POSITIVE == 0) p.addExperience(1);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Fires most of the time when the player is doing things, like if they level up, or break a block, or gain xp, or whatever.
	 * During this event, we keep track of the player's experience level, and ensure that they have the correct number of hearts displayed
	 * on the screen.
	 * @param e
	 */
	@SubscribeEvent
	public void PlayerUpdateEvent(PlayerTickEvent e)
	{
		//Don't do any of this on the client side
		if (e.player instanceof EntityPlayerMP == false) return;
		
		EntityPlayerMP player = (EntityPlayerMP) e.player;
		String name = player.getDisplayName();

		int[] stats = PlayerData.loadStats(name);
		long timer = PlayerData.getStat(Stats.TIMER, stats, true);
		
		//It's sledgehammering time
		if (timer <= 0L) return;
		int level = PlayerData.getStat(Stats.LEVEL, stats);
		
		//Update player levels, disallow positive leveling until all negative levels are gone
		if (level < 0)
		{
			if (player.experienceLevel > 0)
			{
				level += player.experienceLevel;
				PlayerData.setStat(Stats.LEVEL, stats, level);
				player.experienceLevel = 0;
				player.addExperienceLevel(Math.max(0, level));
				ModCore.network.sendTo(new PlayerDataPacket(level, timer, PlayerData.getStat(Stats.PENALTY, stats)), player);
				PlayerData.saveStats(name, stats);
			}			
		}
		else
		{
			if (player.experienceLevel != level)
			{
				level = player.experienceLevel;
				PlayerData.setStat(Stats.LEVEL, stats, level);
				ModCore.network.sendTo(new PlayerDataPacket(level, timer, PlayerData.getStat(Stats.PENALTY, stats)), player);
				PlayerData.saveStats(name, stats);
			}
		}
		
		//Update player healths
		int oldMax = (int) player.getMaxHealth();
		int newMax = calcHealth(level);
		
		//Only require time if it is required
		if (Settings.REQUIRE_TIME_FOR_REGAIN)
		{
			timer = Settings.HP_REGAIN > 0 ? (int) ((float) (player.worldObj.getTotalWorldTime() - timer) / (24000F / Settings.HP_REGAIN)) : 0;
			int timeMax = calcHealth(PlayerData.getStat(Stats.PENALTY, stats)) + (int)(timer << 1);
			if (newMax > timeMax) newMax = timeMax;
		}
		newMax += calcBonusHealth(level);
		
		//More sledge hammers. Going through portals seems to do something funky. This solves the problem.
		player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(20F);
		player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(oldMax);
		
		if (newMax == oldMax) return;
		
		player.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(newMax);
		player.setHealth(newMax > oldMax ? (int) (player.getHealth() + (newMax - oldMax)) : Math.min((int) player.getHealth(), newMax));
	}
	
    /**
     * Calculates the player's <u>health value</u> from their experience level. <b>Health value is 2x number of hearts</b>
     * @param level
     * @return
     */
    public static final int calcHealth(int level){ 	return calcHearts(level) << 1;  }
    
    /**
     * Calculates the player's <u>number of hearts</u> from their experience level
     * @param level
     * @return
     */
    public static final int calcHearts(int level)
    {
    	return Math.max(Settings.HP_BASE_AMOUNT, Math.min(Settings.HP_BASE_MAX, (Settings.HP_BASE_GAIN * ((level - Settings.HP_BASE_LEVEL)/Settings.HP_BASE_STEP)) + Settings.HP_BASE_AMOUNT));
    }
    
    /** Calculate number of bonus hearts*/
    public static final int calcBonusHearts(int level)
    {
    	if (level < Settings.HP_BONUS_LEVEL) return 0;
    	return Math.min(Settings.HP_BONUS_MAX, 1 + ((level - Settings.HP_BONUS_LEVEL)/Settings.HP_BONUS_STEP)*Settings.HP_BONUS_GAIN);
    	
    }
    /** Calculate bonus health value */
    public static final int calcBonusHealth(int level) { return calcBonusHearts(level) << 1; }


}
