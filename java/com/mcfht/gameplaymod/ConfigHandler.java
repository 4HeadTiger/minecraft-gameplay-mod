package com.mcfht.gameplaymod;

import com.mcfht.gameplaymod.levelsnstats.Module_Levels;
import com.mcfht.gameplaymod.scaffolds.Module_Scaffolds;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraftforge.common.config.Configuration;

public class ConfigHandler
{
	public static void handleConfigs(final Configuration config)
	{
		config.load();
		
		Settings.GUI_SHOW_HUD_TWEAKS = config.getBoolean("showModifiedHUD", "3. CLIENT SETTINGS", true, "Enable/Disable the mod's HUD tweaks (the tweaks are helpful, but not vital)");
		Settings.GUI_SHOW_BONUS_HEART_OUTLINES = config.getBoolean("outlineBonusHearts", "3. CLIENT SETTINGS", true, "Whether to outline bonus hearts - disable if they look funny on your texture pack");
		Settings.GUI_SHOW_MISSING_HEART_OUTLINES = config.getBoolean("outlineMissingHearts", "3. CLIENT SETTINGS", true, "Whether to outline missing hearts - disable if they look funny on your texture pack");

		
		///////////// HUNGER //////////////////
		Settings.HUNGER_MIN 							=  config.getInt("hungerMinAtRespawn", "2.HUNGER PENALTIES", 3, 1, 10, "The minimum number of shanks at respawn (hunger carries over from death. Set to 10 for vanilla style hunger).)");
		Settings.HUNGER_MOD 							=  config.getInt("hungerModtAtRespawn", "2.HUNGER PENALTIES", 0, -10, 10, "Adds this to the player's Hunger Shanks at death. Negative means shanks are emptied, while positive means shanks are filled");
		
		///////////// LEVELING STUFF ///////////////////
		Settings.XP_NIGHT_RATE_NEGATIVE 				= config.getInt("nightXPRateNegativeLevels", "2.LEVEL PENALTIES", 10, 0, 0x7FFFFFFF, "Number of seconds between passive night-time xp for negative level players. 0 to disable.");
		Settings.XP_NIGHT_RATE_POSITIVE 				= config.getInt("nightXPRatePositiveLevels", "2.LEVEL PENALTIES", 15, 0, 0x7FFFFFFF, "Number of seconds between passive night-time xp for positive level players. 0 to disable.");

		Settings.LEVEL_LOSS_NEGATIVE_DIMINISH_FACTOR 	= config.getInt("levelLossDiminish", "2.LEVEL PENALTIES", 6, 0, 10, "Scaling value for diminishing level loss for negative levels. Functional minimum level is N * base_loss. CHANGE WITH CARE!");
		Settings.LEVEL_LOSS_POSITIVE_MULT 				= config.getInt("levelLossPositiveScale", "2.LEVEL PENALTIES", 3, 1, 100, "Ratio of positive levels to negative levels lost. N * Base can be lost while player has positive level). CHANGE WITH CARE!");
		Settings.LEVEL_LOSS_NEGATIVE 					= config.getInt("levelLossBase", "2.LEVEL PENALTIES", 10, 0, 100, "The base number of levels lost when dying at level 0.");
		Settings.LEVEL_MINIMUM							= config.getInt("minimumLevel", "2.LEVEL PENALTIES", -60, -1000, 0, "Absolute limit on lowest possible negative level. Set to 0 to disable negative levels");
		Settings.LEVELS_DO_FANCY						= config.getBoolean("doFancyLeveling", "2.LEVEL PENALTIES", true, "Whether to use the fancy level loss/retention mechanics. If false, the player will always spawn at level 0");
		
		Settings.PENALTY_MOD_ENABLED 					= config.getBoolean("enableFancyDeathStuff", "2. DEATH PENALTIES", true, "Enable/Disable Death Penalties Module");		

		//Settings.timerHeartScale 						= (int) ((float)Settings.timerBaseHeartScale/Settings.HP_REGAIN);
		
		////////// HEALTH MOD /////////
		Settings.HP_BONUS_MAX 							= config.getInt("heartsBonusMax", 	"1. BONUS HEARTS", 10, 1, 10, "Max number of super special bonus hearts the player can have");
		Settings.HP_BONUS_STEP 							= config.getInt("heartsBonusLevelStep", "1. BONUS HEARTS", 7, 1, 100, "Number of levels between each step");
		Settings.HP_BONUS_GAIN 							= config.getInt("heartsBonusGainPerStep", "1. BONUS HEARTS", 1, 1, 40, "Number of bonus hearts to gain per step");
		Settings.HP_BONUS_LEVEL						 	= config.getInt("heartsBonusLevel", "1. BONUS HEARTS", 30, -100, 100, "Level at which player starts getting bonus hearts");
		
		Settings.HP_REGAIN 								= config.getFloat("maxHeartsEarnedPerDay", "1.HEALTH SCALING", 1F, 0F, 48F, "The number of heart slots that are unlocked for every MC day survived. Setting to 0 in conjunction with neg levels should make heart loss permanent.!");		
		Settings.REQUIRE_TIME_FOR_REGAIN				= config.getBoolean("alsoRequireTimeForHearts", "1.HEALTH SCALING", true, "Whether time is required to regain lost hearts");
		
		Settings.HP_BASE_AMOUNT						 	= config.getInt("heartsBaseMin", "1.HEALTH SCALING", 3, 1, 40, "Min number of hearts the player can have.");
		Settings.HP_BASE_MAX 							= config.getInt("heartsBaseMax", "1.HEALTH SCALING", 10, 1, 40, "Max number of regular hearts the player can have.");
		Settings.HP_BASE_STEP 							= config.getInt("heartsBaseLevelStep", "1.HEALTH SCALING", 5, 1, 100, "Number of levels between each steap");
		Settings.HP_BASE_GAIN 							= config.getInt("heartsBaseGainPerStep", "1.HEALTH SCALING", 1, 1, 40, "Number of hearts to gain per step");
		Settings.HP_BASE_LEVEL 							= config.getInt("heartsBaseLevel", "1.HEALTH SCALING", -30, -100, 100, "The location of the 0th step. You will gain hearts every 'heartsBaseLevelStep' levels from this point");
		
		Settings.HP_BASE_AMOUNT = Math.max(1, Math.min(Settings.HP_BASE_AMOUNT, Settings.HP_BASE_MAX));
		
		////////////// SPECIAL RESPAWN LOCATION //////////////////
		
		Settings.doSpecialRespawn						= config.getBoolean("alternateRespawnEnabled", "2.RESPAWNING", false, "Whether to enable a special respawn");
		Settings.COORD_RESPAWN_X						= config.getInt("respawnX", "2.RESPAWNING", 0, -30000000, 30000000, "x Coordinate for special respawn");
		Settings.COORD_RESPAWN_Y						= config.getInt("respawnY", "2.RESPAWNING", 65, 0, 256, "y Coordinate for special respawn");
		Settings.COORD_RESPAWN_Z						= config.getInt("respawnZ", "2.RESPAWNING", 0, -30000000, 30000000, "z Coordinate for special respawn");
		Settings.COORD_RESPAWN_DIMENSION				= config.getInt("respawnDimension", "2.RESPAWNING", 0, -16, 16, "Dimension ID for respawn (Change with care, 0 = overworld, nether = -1, end = 1, twilight forest is usually 7, etc");


		for (int i = 0; i < Module_Scaffolds.NUMBER_OF_SCAFFOLDS; i++)
		{
			//Module_Scaffolds.enabled[i] 			= config.getBoolean(Module_Scaffolds.NAMES[i] + "ScaffoldEnabled", "SCAFFOLDS", true, "Enable/Disable this scaffold (note: completely removes from world)");
			Module_Scaffolds.maxHeight[i] 			= config.getInt(Module_Scaffolds.NAMES[i] + "ScaffoldHeight", "3.TYPES", 10 + (i * 10), 1, 256, "Max height of " + Module_Scaffolds.NAMES[i] + " support members");
			Module_Scaffolds.maxBranch[i] 			= config.getInt(Module_Scaffolds.NAMES[i] + "ScaffoldLength", "3.TYPES", 8 + (i << 3), 1, 64, "Max size for " + Module_Scaffolds.NAMES[i] + " scaffold branches");
			Module_Scaffolds.recipeOutput[i]			= config.getInt(Module_Scaffolds.NAMES[i] + "recipeQuantity", "3.TYPES", 4, 0, 64, "Number of " + Module_Scaffolds.NAMES[i] + " scaffolds returned for each crafting recipe. 0 to disable." );
		}
		
		Settings.SCAFFOLDS_ENABLED = config.getBoolean("enableScaffolds", "3.SCAFFOLDS", true, "Enable/Disable scaffolds (note: completely removes them from world)");
		
		config.save();
	}
}