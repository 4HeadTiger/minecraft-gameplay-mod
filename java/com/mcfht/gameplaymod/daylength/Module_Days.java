package com.mcfht.gameplaymod.daylength;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Timer;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.IRenderHandler;
import net.minecraftforge.common.MinecraftForge;

import org.lwjgl.opengl.GL11;

import com.mcfht.gameplaymod.ModCore;
import com.mcfht.gameplaymod.Settings;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Module_Days {

	private static final ResourceLocation locationMoonPhasesPng = new ResourceLocation("textures/environment/moon_phases.png");
    private static final ResourceLocation locationSunPng = new ResourceLocation("textures/environment/sun.png");
    private static final ResourceLocation locationCloudsPng = new ResourceLocation("textures/environment/clouds.png");
    private static final ResourceLocation locationEndSkyPng = new ResourceLocation("textures/environment/end_sky.png");
	public static World theWorld;
	public static TextureManager renderEngine;
	
	public static int starGLCallList;
	public static int glSkyList, glSkyList2;
	public static boolean isInit = false;
	
	public static void init()
	{
		
		try {
			Field field = Minecraft.getMinecraft().getClass().getDeclaredField("timer");
			field.setAccessible(true);
			timer = (Timer) field.get(Minecraft.getMinecraft());
		} catch (Exception e){
			System.err.println("GAMEPLAY MOD/DAYLENGTH MOD FAILED TO GRAB CLIENT RENDER TIMERS. FORCING CRASH!!!");
			int n = 1/0;
		}
		
		
		renderEngine = Minecraft.getMinecraft().getTextureManager();
		starGLCallList = GLAllocation.generateDisplayLists(3);
		starGLCallList = GLAllocation.generateDisplayLists(3);
        GL11.glPushMatrix();
        GL11.glNewList(starGLCallList, GL11.GL_COMPILE);
        renderStars();
        GL11.glEndList();
        GL11.glPopMatrix();
        Tessellator tessellator = Tessellator.instance;
        glSkyList = starGLCallList + 1;
        GL11.glNewList(glSkyList, GL11.GL_COMPILE);
        byte b2 = 64;
        int i = 256 / b2 + 2;
        float f = 16.0F;
        int j;
        int k;

        for (j = -b2 * i; j <= b2 * i; j += b2)
        {
            for (k = -b2 * i; k <= b2 * i; k += b2)
            {
                tessellator.startDrawingQuads();
                tessellator.addVertex((double)(j + 0), (double)f, (double)(k + 0));
                tessellator.addVertex((double)(j + b2), (double)f, (double)(k + 0));
                tessellator.addVertex((double)(j + b2), (double)f, (double)(k + b2));
                tessellator.addVertex((double)(j + 0), (double)f, (double)(k + b2));
                tessellator.draw();
            }
        }

        GL11.glEndList();
        glSkyList2 = starGLCallList + 2;
        GL11.glNewList(glSkyList2, GL11.GL_COMPILE);
        f = -16.0F;
        tessellator.startDrawingQuads();

        for (j = -b2 * i; j <= b2 * i; j += b2)
        {
            for (k = -b2 * i; k <= b2 * i; k += b2)
            {
                tessellator.addVertex((double)(j + b2), (double)f, (double)(k + 0));
                tessellator.addVertex((double)(j + 0), (double)f, (double)(k + 0));
                tessellator.addVertex((double)(j + 0), (double)f, (double)(k + b2));
                tessellator.addVertex((double)(j + b2), (double)f, (double)(k + b2));
            }
        }

        tessellator.draw();
        GL11.glEndList();
	        
		//glSkyList = starGLCallList + 1;
		//glSkyList2 = starGLCallList + 2;
		theWorld = Minecraft.getMinecraft().theWorld;
		isInit = true;
	}
	
	/**
	 * Creates geometry for the stars
	 */
	private static void renderStars()
    {
        Random random = new Random((long)Settings.STAR_DENSITY);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();

        for (int i = 0; i < Settings.STAR_DENSITY; ++i)
        {
            double d0 = (double)(random.nextFloat() * 1.8F - 0.9F);
            double d1 = (double)(random.nextFloat() * 1.4F - 0.7F);
            double d2 = (double)(random.nextFloat() * 2.0F - 1.0F);
            double d3 = (double)(0.08F + random.nextFloat() * 0.13F);
            double d4 = d0 * d0 + d1 * d1 + d2 * d2;

            if (d4 < 1.3D && d4 > 0.001D)
            {
                d4 = 1.0D / Math.sqrt(d4);
                
                d0 *= d4;
                d1 *= d4;
                d2 *= d4;
                
                double d5 = d0 * 100.0D;
                double d6 = d1 * 100.0D;
                double d7 = d2 * 100.0D;
                double d8 = Math.atan2(d0, d2);
                double d9 = Math.sin(d8);
                double d10 = Math.cos(d8);
                double d11 = Math.atan2(Math.sqrt(d0 * d0 + d2 * d2), d1);
                double d12 = Math.sin(d11);
                double d13 = Math.cos(d11);
                double d14 = random.nextDouble() * Math.PI * 2.0D;
                double d15 = Math.sin(d14);
                double d16 = Math.cos(d14);

                for (int j = 0; j < 4; ++j)
                {
                    double d17 = 0.0D;
                    double d18 = (double)((j & 2) - 1) * d3;
                    double d19 = (double)((j + 1 & 2) - 1) * d3;
                    double d20 = d18 * d16 - d19 * d15;
                    double d21 = d19 * d16 + d18 * d15;
                    double d22 = d20 * d12 + d17 * d13;
                    double d23 = d17 * d12 - d20 * d13;
                    double d24 = d23 * d9 - d21 * d10;
                    double d25 = d21 * d9 + d23 * d10;
                    tessellator.addVertex(d5 + d24, d6 + d22, d7 + d25);
                }
            }
        }

        tessellator.draw();
    }

	
	protected static double clientCounter = 0;
	protected static int clientDayCounter = 0;
	
	//protected static long lastWorldTime;
	//protected static float stepFraction;
	public static net.minecraft.util.Timer timer;
	
	
	public static void renderSky(float tickFraction)
    {
		
		if (isInit == false) init();
		
		//tickFraction = 0F;
				
		IRenderHandler skyProvider = null;
		theWorld = Minecraft.getMinecraft().theWorld;		
		if (theWorld == null) return;
		double actualTime;
		
		
		if (timer.elapsedTicks != 0 && Minecraft.getMinecraft().isGamePaused() == false)
		{
			clientCounter = incrementCounter(clientCounter, timer.elapsedTicks);
			while (clientCounter > 24000D)
			{
				clientCounter -= 24000D;
				clientDayCounter++;
			}
			//On the integrated server, this ensures consistency. On the dedicated server,
			//this does nothing
			Settings.dayTimeCounter = clientCounter;
			Settings.daysPassed = clientDayCounter;
		}
		//Calculate the actual world time as a percentage of a total day
		actualTime = ((double)clientCounter / 24000D) + (((double)timer.renderPartialTicks * Settings.TIME_SCALE)/24000D);

		
		//System.out.println(tickFraction + ", " + ((double)theWorld.getWorldTime() / 24000D) + ", " + actualTime);
		if (actualTime > 1D) actualTime -= 1D;
		if (actualTime < 0D) actualTime += 1D;
		/*
        if ((skyProvider = theWorld.provider.getSkyRenderer()) != null)
        {
            skyProvider.render(tickFraction, Minecraft.getMinecraft().theWorld, Minecraft.getMinecraft());
            return;
        }*/
        
        if (Minecraft.getMinecraft().theWorld.provider.dimensionId == 1)
        {
            GL11.glDisable(GL11.GL_FOG);
            GL11.glDisable(GL11.GL_ALPHA_TEST);
            GL11.glEnable(GL11.GL_BLEND);
            
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            RenderHelper.disableStandardItemLighting();
            GL11.glDepthMask(false);
            
            renderEngine.bindTexture(locationEndSkyPng);
            Tessellator tessellator = Tessellator.instance;

            for (int i = 0; i < 6; ++i)
            {
                GL11.glPushMatrix();

                if (i == 1)
                {
                    GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
                }

                if (i == 2)
                {
                    GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
                }

                if (i == 3)
                {
                    GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
                }

                if (i == 4)
                {
                    GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
                }

                if (i == 5)
                {
                    GL11.glRotatef(-90.0F, 0.0F, 0.0F, 1.0F);
                }

                tessellator.startDrawingQuads();
                tessellator.setColorOpaque_I(0x282828);
                tessellator.addVertexWithUV(-100.0D, -100.0D, -100.0D, 0.0D, 0.0D);
                tessellator.addVertexWithUV(-100.0D, -100.0D, 100.0D, 0.0D, 16.0D);
                tessellator.addVertexWithUV(100.0D, -100.0D, 100.0D, 16.0D, 16.0D);
                tessellator.addVertexWithUV(100.0D, -100.0D, -100.0D, 16.0D, 0.0D);
                tessellator.draw();
                GL11.glPopMatrix();
            }

            GL11.glDepthMask(true);
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glEnable(GL11.GL_ALPHA_TEST);
        }
        else if (theWorld.provider.isSurfaceWorld())
        {
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            
            Vec3 vec3 = getSkyColorBody(theWorld, Minecraft.getMinecraft().renderViewEntity, actualTime, tickFraction);

            float f1 = (float)vec3.xCoord;
            float f2 = (float)vec3.yCoord;
            float f3 = (float)vec3.zCoord;
            float f6;

            if (Minecraft.getMinecraft().gameSettings.anaglyph)
            {
                float f4 = (f1 * 30.0F + f2 * 59.0F + f3 * 11.0F) / 100.0F;
                float f5 = (f1 * 30.0F + f2 * 70.0F) / 100.0F;
                f6 = (f1 * 30.0F + f3 * 70.0F) / 100.0F;
                f1 = f4;
                f2 = f5;
                f3 = f6;
            }

            GL11.glColor3f(f1, f2, f3);
            
            Tessellator tessellator1 = Tessellator.instance;
            GL11.glDepthMask(false);
            GL11.glEnable(GL11.GL_FOG);
            GL11.glColor3f(f1, f2, f3);
            GL11.glCallList(glSkyList);
            GL11.glDisable(GL11.GL_FOG);
            GL11.glDisable(GL11.GL_ALPHA_TEST);
            GL11.glEnable(GL11.GL_BLEND);
            
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            RenderHelper.disableStandardItemLighting();
            
            float[] sunsetColors = theWorld.provider.calcSunriseSunsetColors((float) getSolarAngleFraction(actualTime), tickFraction);
            float f7;
            float f8;
            float f9;
            float f10; 

            if (sunsetColors != null)
            {
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GL11.glShadeModel(GL11.GL_SMOOTH);
                GL11.glPushMatrix();
                GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
                
                GL11.glRotatef(MathHelper.sin((float) getSolarAngleRadians(actualTime)) < 0.0F ? 180.0F : 0.0F, 0.0F, 0.0F, 1.0F);
                GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
                
                f6 = sunsetColors[0];
                f7 = sunsetColors[1];
                f8 = sunsetColors[2];
                float f11;

                if (Minecraft.getMinecraft().gameSettings.anaglyph)
                {
                    f9 = (f6 * 30.0F + f7 * 59.0F + f8 * 11.0F) / 100.0F;
                    f10 = (f6 * 30.0F + f7 * 70.0F) / 100.0F;
                    f11 = (f6 * 30.0F + f8 * 70.0F) / 100.0F;
                    f6 = f9;
                    f7 = f10;
                    f8 = f11;
                }

                tessellator1.startDrawing(6);
                tessellator1.setColorRGBA_F(f6, f7, f8, sunsetColors[3]);
                tessellator1.addVertex(0.0D, 100.0D, 0.0D);
                byte b0 = 16;
                tessellator1.setColorRGBA_F(sunsetColors[0], sunsetColors[1], sunsetColors[2], 0.0F);

                for (int j = 0; j <= b0; ++j)
                {
                    f11 = (float)j * (float)Math.PI * 2.0F / (float)b0;
                    float f12 = MathHelper.sin(f11);
                    float f13 = MathHelper.cos(f11);
                    tessellator1.addVertex((double)(f12 * 120.0F), (double)(f13 * 120.0F), (double)(-f13 * 40.0F * sunsetColors[3]));
                }

                tessellator1.draw();
                GL11.glPopMatrix();
                GL11.glShadeModel(GL11.GL_FLAT);
            }

            GL11.glEnable(GL11.GL_TEXTURE_2D);
            OpenGlHelper.glBlendFunc(770, 1, 1, 0);
            GL11.glPushMatrix();
            f6 = 1.0F - theWorld.getRainStrength(tickFraction);
            f7 = 0.0F;
            f8 = 0.0F;
            f9 = 0.0F;
            GL11.glColor4f(1.0F, 1.0F, 1.0F, f6);
            GL11.glTranslatef(f7, f8, f9);
            
            
            GL11.glRotatef(-90.0F + (float)(getSunTiltY(clientDayCounter, clientCounter) * 28F), 0.0F, 1.0F, 0.0F);
            GL11.glRotatef((float)(getSunTiltZ(clientDayCounter, clientCounter) * 28F), 0.0F, 0.0F, 1.0F);
            
            GL11.glRotatef((float) getCalcSolarAngleWithStretch(clientDayCounter, actualTime), 1.0F, 0.0F, 0.0F);
            f10 = 30.0F;
            
            renderEngine.bindTexture(locationSunPng);
            tessellator1.startDrawingQuads();
            
            tessellator1.addVertexWithUV((double)(-f10), 100.0D, (double)(-f10), 0.0D, 0.0D);
            tessellator1.addVertexWithUV((double)f10, 100.0D, (double)(-f10), 1.0D, 0.0D);
            tessellator1.addVertexWithUV((double)f10, 100.0D, (double)f10, 1.0D, 1.0D);
            tessellator1.addVertexWithUV((double)(-f10), 100.0D, (double)f10, 0.0D, 1.0D);
            tessellator1.draw();
            f10 = 20.0F;
            
            renderEngine.bindTexture(locationMoonPhasesPng);
            
            int k = theWorld.getMoonPhase();
            int l = k & 0x3;
            int i1 = (k >> 2) & 0x1;;
            float f14 = (float)(l + 0) / 4.0F;
            float f15 = (float)(i1 + 0) / 2.0F;
            float f16 = (float)(l + 1) / 4.0F;
            float f17 = (float)(i1 + 1) / 2.0F;
            
            GL11.glRotatef((float)(getSunTiltZ(clientDayCounter, clientCounter) * 12.5F), 0.0F, 1.0F, 0.0F);
            GL11.glRotatef((float)(getSunTiltY(clientDayCounter, clientCounter) * 12.5F), 0.0F, 0.0F, 1.0F);
            
            //GL11.glRotatef((float) getSolarAngleFraction(actualTime) * 360.0F + (float) (clientDayCounter & 20), 1.0F, 0.0F, 0.0F);
            
            tessellator1.startDrawingQuads();
            tessellator1.addVertexWithUV((double)(-f10), -100.0D, (double)f10, (double)f16, (double)f17);
            tessellator1.addVertexWithUV((double)f10, -100.0D, (double)f10, (double)f14, (double)f17);
            tessellator1.addVertexWithUV((double)f10, -100.0D, (double)(-f10), (double)f14, (double)f15);
            tessellator1.addVertexWithUV((double)(-f10), -100.0D, (double)(-f10), (double)f16, (double)f15);
            tessellator1.draw();
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            float f18 = getStarBrightnessBody(actualTime) * f6;
            
            if (f18 > 0.0F)
            {
                GL11.glColor4f(f18, f18, f18, f18);
                GL11.glCallList(starGLCallList);
            }

            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL11.GL_ALPHA_TEST);
            GL11.glEnable(GL11.GL_FOG);
            GL11.glPopMatrix();
            GL11.glDisable(GL11.GL_TEXTURE_2D);
            GL11.glColor3f(0.0F, 0.0F, 0.0F);
            
            double d0 = Minecraft.getMinecraft().thePlayer.getPosition(tickFraction).yCoord - theWorld.getHorizon();

            if (d0 < 0.0D)
            {
                GL11.glPushMatrix();
                GL11.glTranslatef(0.0F, 12.0F, 0.0F);
                GL11.glCallList(glSkyList2);
                GL11.glPopMatrix();
                f8 = 1.0F;
                f9 = -((float)(d0 + 65.0D));
                f10 = -f8;
                tessellator1.startDrawingQuads();
                tessellator1.setColorRGBA_I(0, 255);
                tessellator1.addVertex((double)(-f8), (double)f9, (double)f8);
                tessellator1.addVertex((double)f8, (double)f9, (double)f8);
                tessellator1.addVertex((double)f8, (double)f10, (double)f8);
                tessellator1.addVertex((double)(-f8), (double)f10, (double)f8);
                tessellator1.addVertex((double)(-f8), (double)f10, (double)(-f8));
                tessellator1.addVertex((double)f8, (double)f10, (double)(-f8));
                tessellator1.addVertex((double)f8, (double)f9, (double)(-f8));
                tessellator1.addVertex((double)(-f8), (double)f9, (double)(-f8));
                tessellator1.addVertex((double)f8, (double)f10, (double)(-f8));
                tessellator1.addVertex((double)f8, (double)f10, (double)f8);
                tessellator1.addVertex((double)f8, (double)f9, (double)f8);
                tessellator1.addVertex((double)f8, (double)f9, (double)(-f8));
                tessellator1.addVertex((double)(-f8), (double)f9, (double)(-f8));
                tessellator1.addVertex((double)(-f8), (double)f9, (double)f8);
                tessellator1.addVertex((double)(-f8), (double)f10, (double)f8);
                tessellator1.addVertex((double)(-f8), (double)f10, (double)(-f8));
                tessellator1.addVertex((double)(-f8), (double)f10, (double)(-f8));
                tessellator1.addVertex((double)(-f8), (double)f10, (double)f8);
                tessellator1.addVertex((double)f8, (double)f10, (double)f8);
                tessellator1.addVertex((double)f8, (double)f10, (double)(-f8));
                tessellator1.draw();
            }

            if (theWorld.provider.isSkyColored())
            {
                GL11.glColor3f(f1 * 0.2F + 0.04F, f2 * 0.2F + 0.04F, f3 * 0.6F + 0.1F);
            }
            else
            {
                GL11.glColor3f(f1, f2, f3);
            }

            GL11.glPushMatrix();
            GL11.glTranslatef(0.0F, -((float)(d0 - 16.0D)), 0.0F);
            GL11.glCallList(glSkyList2);
            GL11.glPopMatrix();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            GL11.glDepthMask(true);
        }
    }
	
	public static double getActualTime()
	{
		if (timer == null) return 0F;
		return ((double)clientCounter + ((double)timer.renderPartialTicks * Settings.TIME_SCALE))/24000D;
	}

	public static float getSolarAngleFractionMask(float tickFraction){ return (float)getSolarAngleFraction(getActualTime()); }
	public static float getSolarAngleFractionMask2(World w, float tickFraction){ return (float)getSolarAngleFraction(getActualTime()); }

	public static double getSolarAngleFraction(double actualTime)
	{
		double d0 = actualTime - 0.25D;
		if (d0 < 0D) d0 += 1D;
		double d1 = d0;
        d0 = 1.0D - ((Math.cos(d0 * Math.PI) + 1.0D) / 2.0D);
		return d1 + (d0 - d1) / 3.0F;
	}
	
	public static float getCalcSolarAngleWithStretch(int days, double actualTime)
	{
		double x = (double)Settings.daysPassed + actualTime;
		x = actualTime + (Math.sin(x/4D) * 0.1D);
		if (x < 0) x += 1D;
		if (x > 1D) x -= 1D;
		return (float) (getSolarAngleFraction(x) * 360F);
	}
	
	public static float getSolarAngleRadiansMask(float tickFraction){	return (float)getSolarAngleRadians(getActualTime()); }
	public static double getSolarAngleRadians(double actualTime)
	{
		return getSolarAngleFraction(actualTime) * 2D * Math.PI;
	}
	
	@SideOnly(Side.CLIENT)
	public static Vec3 getSkyColorMask(Entity viewer, float tickFraction)
	{
		if (viewer == null || viewer.worldObj == null) return Vec3.createVectorHelper(1D, 1D, 1D); 
		return getSkyColorBody(viewer.worldObj, viewer, getActualTime(), tickFraction);
	}
	
    @SideOnly(Side.CLIENT)
    public static Vec3 getSkyColorBody(World w, Entity viewer, double actualTime, float tickFraction)
    {
    	
        float f1 = (float) getSolarAngleFraction(actualTime);
        
        float f2 = MathHelper.cos(f1 * (float)Math.PI * 2.0F) * 2.0F + 0.5F;

        if (f2 < 0.0F)
        {
            f2 = 0.0F;
        }

        if (f2 > 1.0F)
        {
            f2 = 1.0F;
        }

        int i = MathHelper.floor_double(viewer.posX);
        int j = MathHelper.floor_double(viewer.posY);
        int k = MathHelper.floor_double(viewer.posZ);
        
        int l = ForgeHooksClient.getSkyBlendColour(w, i, j, k);
        
        float f4 = (float)(l >> 16 & 255) / 255.0F;
        float f5 = (float)(l >> 8 & 255) / 255.0F;
        float f6 = (float)(l & 255) / 255.0F;
        f4 *= f2;
        f5 *= f2;
        f6 *= f2;
        float f7 = w.getRainStrength(tickFraction);
        float f8;
        float f9;

        if (f7 > 0.0F)
        {
            f8 = (f4 * 0.3F + f5 * 0.59F + f6 * 0.11F) * 0.6F;
            f9 = 1.0F - f7 * 0.75F;
            f4 = f4 * f9 + f8 * (1.0F - f9);
            f5 = f5 * f9 + f8 * (1.0F - f9);
            f6 = f6 * f9 + f8 * (1.0F - f9);
        }

        f8 = w.getWeightedThunderStrength(tickFraction);

        if (f8 > 0.0F)
        {
            f9 = (f4 * 0.3F + f5 * 0.59F + f6 * 0.11F) * 0.2F;
            float f10 = 1.0F - f8 * 0.75F;
            f4 = f4 * f10 + f9 * (1.0F - f10);
            f5 = f5 * f10 + f9 * (1.0F - f10);
            f6 = f6 * f10 + f9 * (1.0F - f10);
        }

        if (w.lastLightningBolt > 0)
        {
            f9 = (float)w.lastLightningBolt - tickFraction;

            if (f9 > 1.0F)
            {
                f9 = 1.0F;
            }

            f9 *= 0.45F;
            f4 = f4 * (1.0F - f9) + 0.8F * f9;
            f5 = f5 * (1.0F - f9) + 0.8F * f9;
            f6 = f6 * (1.0F - f9) + 1.0F * f9;
        }

        return Vec3.createVectorHelper((double)f4, (double)f5, (double)f6);
    }
	
	
    public static double getSunTiltY(int days, double time)
    {
    	double x = (double)days + (time/24000D);
    	while (x > 30) x -= 30;
    	return Math.sin((x/Math.PI)); 
    }
    public static double getSunTiltZ(int days, double time)
    {
    	double x = (double)days + (time/24000D);
    	while (x > 30) x -= 30;
    	return Math.sin((x/Math.PI) + Math.PI/2D); 
    }
    
    @SideOnly(Side.CLIENT)
    public static float getStarBrightnessBody(double actualTime)
    {
        float f1 = (float) getSolarAngleFraction(actualTime);
        float f2 = 1.0F - (MathHelper.cos(f1 * (float)Math.PI * 2.0F) * 2.0F + 0.25F);

        if (f2 < 0.0F)
        {
            f2 = 0.0F;
        }

        if (f2 > 1.0F)
        {
            f2 = 1.0F;
        }

        return f2 * f2 * 0.55F;
    }
	
	
	public static Map<Integer, Long> worldTimePrevious = new HashMap<Integer, Long>();
	@SubscribeEvent
	public void serverTickEvent(ServerTickEvent e)
	{
		if (e.phase == Phase.END)
		{
			
			Settings.dayTimeCounter = incrementCounter(Settings.dayTimeCounter);
			
			while (Settings.dayTimeCounter > 24000D)
			{
				Settings.dayTimeCounter -= 24000D;
				Settings.daysPassed++;
			}
			
			for (World w : MinecraftServer.getServer().worldServers)
			{
				long lastTime = worldTimePrevious.get(w.provider.dimensionId);
				if (w.getWorldTime() != lastTime)
				{
					Settings.dayTimeCounter = w.getWorldTime();
					while (Settings.dayTimeCounter > 24000D)
					{
						Settings.dayTimeCounter -= 24000D;
						Settings.daysPassed++;
					}
					ModCore.network.sendToAll(new TimeUpdatePacket(true));
				}
				else w.setWorldTime((long)Settings.dayTimeCounter);
			}
		}
		else
		{
			for (World w : MinecraftServer.getServer().worldServers)
			{
				worldTimePrevious.put(w.provider.dimensionId, (long)Settings.dayTimeCounter);
				w.setWorldTime((long)Settings.dayTimeCounter);
			}
		}

	}
	
	public static double calculateDayStretch(long totalWorldDays)
	{
		return ((float)(totalWorldDays % (int)(Settings.dayShiftMax * 200F))/100F) - Settings.dayShiftMax;
	}
	
	
	public static double incrementCounter(double counter)
	{
		return incrementCounter(counter, 1);
	}
	public static double incrementCounter(double counter, int times)
	{
		counter += Settings.TIME_SCALE * times;
		return counter;
	}
	
}


