package com.mcfht.gameplaymod.daylength;

import com.mcfht.gameplaymod.Settings;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class TimeUpdatePacket  implements IMessage
{
    boolean receiverClient = false;

    public TimeUpdatePacket() { receiverClient = true; }
    public TimeUpdatePacket(boolean isSendingFromServer) { receiverClient = isSendingFromServer; }

    @Override
    public void fromBytes(ByteBuf buf) 
    {
    	receiverClient = buf.readBoolean();
    	if (this.receiverClient)
    	{
    		Module_Days.clientDayCounter = buf.readInt();
    		Module_Days.clientCounter = buf.readDouble();
    	}
    }
    @Override
    public void toBytes(ByteBuf buf) 
    {
    	buf.writeBoolean(this.receiverClient);
    	if (this.receiverClient)
    	{
    		buf.writeInt(Settings.daysPassed);
    		buf.writeDouble(Settings.dayTimeCounter);
    	}

    }
    public static class HandlerClient implements IMessageHandler<TimeUpdatePacket, IMessage> 
    {
        @Override
        public IMessage onMessage(TimeUpdatePacket message, MessageContext ctx) 
        {
            return null;
        }
    }
    public static class HandlerServer implements IMessageHandler<TimeUpdatePacket, IMessage> 
    {
        @Override
        public IMessage onMessage(TimeUpdatePacket message, MessageContext ctx) 
        {
            return new TimeUpdatePacket(true);
        }
    }
}



