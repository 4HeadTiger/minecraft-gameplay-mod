package com.mcfht.gameplaymod.scaffolds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;

import com.mcfht.gameplaymod.ModCore;
import com.mcfht.gameplaymod.Util;
import com.mcfht.gameplaymod.Util.BlockPos;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;


/**
 * The scaffolds module.
 * 
 * There are three tiers of scaffolding. Each tier has advantages and disadvantages.
 * 
 * Simple mode:
 * Scaffold blocks can exist only a limited distance from a supporting member.
 * 
 * Advanced mode:
 * The wooden scaffolds can be built on less stable terrain and are cheaper, however their maximum
 * size is greatly restricted compared to the metal scaffolds. Iron and Steel are similar to one another, however
 * steel is slightly stronger. These two scaffolds will need proper foundations in order to achieve their maximum size.
 * 
 * Each scaffold has two blocks; vertical blocks, and branch blocks. The 0 meta values are reserved
 * for the origin blocks (support members, and blocks connected directly to support members). This means
 * that branches can be a max of 16 blocks in length, in any direction. The support block is the base
 * block that is returned by block picking and so on.
 * 
 * Ground support is fairly simple. We create an array equal to the volume of the testing area, next,
 * we walk down through the blocks and populate the smaller array with the strength values of
 * any blocks that are connected to the origin block. The results of the walk will determine the
 * strength of the structure. A few random blocks around the area will also be sampled, allowing us to extrapolate
 * and guess what the terrain around us looks like. A base block is required to achieve the max height for metal scaffolding.
 * 
 * @author FHT
 *
 */
public class Module_Scaffolds {

	public static final int NUMBER_OF_SCAFFOLDS = 3;
	
	/////////////// USER CONFIGURABLE SETTINGS ///////////////////
	
	/** The number of item outputs for each scaffold (see config handler) */
	public static int recipeOutput[] = new int[NUMBER_OF_SCAFFOLDS];
	///** Enable or disable a specific scaffold*/
	//public static boolean enabled[] = new boolean[NUMBER_OF_SCAFFOLDS];
	/** Height of a specific scaffold*/
	public static int maxHeight[] = new int[NUMBER_OF_SCAFFOLDS];
	/** Length of a specific scaffold*/
	public static int maxBranch[] = new int[NUMBER_OF_SCAFFOLDS];
	
	/**
	 * Whether to accommodate terrain stability into integrity of scaffold
	 */
	public static boolean enableTerrainTesting = true;
	
	////////////// INTERNAL SETTINGS ////////////////////
	
	/** The basic names of the scaffolds. The textures should be named; "scaffold" + name */
	public static final String NAMES[] = {"wood", "iron", "steel"};
	public static final int weights[] = {2, 5, 5};
	/** BlockMaterials for the scaffolds */
	public static final Material material[] = {Material.wood, Material.rock, Material.rock};


	//////////////// IDEALY BE CAREFUL IF YOU TOUCH ANYTHING FROM BELOW THIS POINT //////////////////
	
	/** A map linking the scaffold extension blocks to the collection of blocks for that scaffold type.
	 * <p><b>Internal use only.</b>
	 *  */
	public static Map<Block, Block[]> scaffolds = new HashMap<Block, Block[]>();
	public static Block primaryBlocks[] = new BlockScaffold[NUMBER_OF_SCAFFOLDS];
	
	public Module_Scaffolds()
	{
		
		//if (enableTerrainTesting) Util.initScaffoldPhysicsStuffs();
		
		
		for (int i = 0; i < NUMBER_OF_SCAFFOLDS; i++)
			registerNewScaffold(i, material[i], 0.3F, NAMES[i], maxBranch[i], maxHeight[i], weights[i]);
	
		
		
		//Add the recipes :D
		if (recipeOutput[0] > 0)
		{
			ItemStack is = new ItemStack(primaryBlocks[0], recipeOutput[0]);
			IRecipe r = new ShapedOreRecipe(is, false, new Object[]{"ILI", "L L", "ILI", Character.valueOf('I'), "plankWood", Character.valueOf('L'), Blocks.ladder}); 
			CraftingManager.getInstance().getRecipeList().add(r);
		}
		if (recipeOutput[1] > 0)
		{
			ItemStack is = new ItemStack(primaryBlocks[1], recipeOutput[1]);
			IRecipe r = new ShapedOreRecipe(is, false, new Object[]{"ILI", "L L", "ILI", Character.valueOf('I'), "ingotIron", Character.valueOf('L'), Blocks.ladder}); 
			CraftingManager.getInstance().getRecipeList().add(r);
		}
		if (recipeOutput[2] > 0)
		{
			ItemStack is = new ItemStack(primaryBlocks[2], recipeOutput[2]);
			IRecipe r;

			//Add a recipe to enable the use of the steel scaffold when steel is not available
			if (OreDictionary.getOres("ingotSteel") == null || OreDictionary.getOres("ingotSteel").size() <= 0) 
			{
				r =	new ShapedOreRecipe(is, false, new Object[]{"ILI", "LBL", "ILI", Character.valueOf('I'), "ingotIron", Character.valueOf('L'), Blocks.ladder, 'B', Items.blaze_powder}) ;
				CraftingManager.getInstance().getRecipeList().add(r);
			}
			r = new ShapedOreRecipe(is, false, new Object[]{"ILI", "L L", "ILI", Character.valueOf('I'), "ingotSteel", Character.valueOf('L'), Blocks.ladder}); 
			CraftingManager.getInstance().getRecipeList().add(r);
		}
		
	/*
		if (recipeOutput[1] > 0)
		{
			ItemStack is = new ItemStack(primaryBlocks[1], recipeOutput[1]);
			IRecipe r = new ShapedOreRecipe(is, recipes[1]); 
			CraftingManager.getInstance().getRecipeList().add(r);
		}
	*/
	
	}	
	
	/**
	 * Registers a new scaffold block, sets up the required extension blocks, and so on.
	 * @param number => the index number for name, recipe, settings, etc
	 * @param material
	 * @param hardness
	 * @param blockName
	 * @param maxBranch
	 * @param maxHeight
	 * @param recipe
	 */
	public static void registerNewScaffold(int number, Material material, float hardness, String blockName, int maxBranch, int maxHeight, int weight)
	{
		//Initialize the collection of blocks for this scaffold, and set the primary block
		Block scaffoldBlocks[] = new Block[1 + (maxBranch >> 4)];
		
		//Register the vertical and branch blocks
		scaffoldBlocks[0] = new BlockScaffold(material, maxHeight, maxBranch).setCreativeTab(CreativeTabs.tabBlock).setBlockName("scaffold" + blockName).setHardness(hardness).setBlockTextureName(ModCore.MODID + ":" + "scaffold" + blockName);
		
		for (int i = 0; i < maxBranch >> 4; i++)
		{
			scaffoldBlocks[i + 1] = new BlockScaffold(material, maxHeight, maxBranch, i + 1).setCreativeTab(CreativeTabs.tabBlock).setHardness(hardness).setBlockTextureName(ModCore.MODID + ":" + "scaffold" + blockName);
		}
		
		
		//Now map then into the right spots
		primaryBlocks[number] = scaffoldBlocks[0];
		for (int j = 0; j < scaffoldBlocks.length; j++)
		{
			scaffolds.put(scaffoldBlocks[j], scaffoldBlocks);
			GameRegistry.registerBlock(scaffoldBlocks[j], "scaffold" + blockName + String.valueOf(j));
		}
	}

	
}
