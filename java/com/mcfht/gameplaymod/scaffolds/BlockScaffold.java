package com.mcfht.gameplaymod.scaffolds;

import java.util.HashSet;
import java.util.Random;

import com.mcfht.gameplaymod.Util;
import com.mcfht.gameplaymod.Util.BlockPos;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;


import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;


/**
 * Absurdly over-complicated scaffold algorithm. Design goal: allow efficient traceback to nearest support member
 * 
 * Future goals - implement simple block physics on scaffold structure.
 * 
 * TODO: Make scaffold destruction only remove blocks that are not in range of a support member?
 * 
 * 
 * @author FHT
 *
 */
public class BlockScaffold extends Block {

	
	
	public int maxHeight; public int maxBranch;
	public String registerName;
	public static int tierCounter = 0;
	public final int branchElement;
	public final int tier;
	public BlockScaffold(Material m, int height, int branch)
	{
		this(m, height, branch, 0);
	}
	public BlockScaffold(Material m, int height, int branch, int branchElement)
	{
		super(m);
		this.tier = tierCounter++;
		setBlockBounds(0.01F, 0F, 0.01F, 0.99F, 1F, 0.99F);
		this.branchElement = branchElement;
		this.maxHeight = height; this.maxBranch = branch;
	}
	public BlockScaffold setBlockRegistryName(String name)
	{
		this.registerName = name;
		return this;
	}

	@Override
	public boolean onBlockActivated(World w, int x0, int y0, int z0, EntityPlayer p, int par6, float par7, float par8, float par9)
	{
		//Really bizarre sledgehammer, the JVM (at least mine) doesn't seem to run the method if it doesn't have this line
		if (maxBranch < 0) System.out.println("Wut?");
		
		if (p.isSneaking()) return false;
		
		ItemStack is0 = p.inventory.mainInventory[p.inventory.currentItem];
		if (is0 == null || is0.getItem() == null) return false;
		Block bHeld = Block.getBlockFromItem(is0.getItem());
		if (bHeld == null || bHeld instanceof BlockScaffold == false || !isSameScaffold(this, bHeld)) return false;

		//Now calculate the distance. If we are at distance 0, search for the top of the stack.
		int dist0 = getActualValue(this, w.getBlockMetadata(x0, y0, z0));
		if (dist0 == 0)
		{
			//First, count the blocks below, then the ones above.
			int columnSize = 0;
			while (w.getBlock(x0,y0-columnSize,z0) instanceof BlockScaffold) columnSize++;
			int maxAllowed = (w.getBlock(x0, y0 - columnSize, z0) instanceof BlockScaffold == false) ? maxHeight : ((BlockScaffold)w.getBlock(x0, y0 - columnSize, z0)).maxHeight;

			int yN = y0;
			while (columnSize < maxHeight && isSameScaffold(this, w.getBlock(x0, yN, z0)))
			{
				yN++;
				columnSize++;
				if (yN >= 255) break;
			}
			if (w.getBlock(x0, yN, z0) == Blocks.air && (p.posX < x0 + 0.02F || p.posX > x0 + 0.98F || p.posZ < z0 + 0.02F || p.posZ > z0 + 0.98F || p.posY < yN - 2 || p.posY > yN + 2))
			{
				w.setBlock(x0, yN, z0, this);
				if (!p.capabilities.isCreativeMode && --is0.stackSize <= 0) p.inventory.setInventorySlotContents(p.inventory.currentItem, null);
				return true;
			}
		}
		else
		{
			//Try to stack scaffolds underneath
			boolean inHorizontalBounds = (p.posX > x0 && p.posX < x0+1 && p.posZ > z0 && p.posZ < z0+1 && p.posY > y0 && p.posY < y0 + 3F);
			
			Block bA = w.getBlock(x0, y0 - 1, z0);
			if (inHorizontalBounds && (bA == Blocks.air || ((isSameScaffold(this, bA) && getActualValue(this, w.getBlockMetadata(x0, y0 - 1, z0)) > dist0))))
			{
				int yN = y0;
				while (dist0 < maxBranch && isSameScaffold(this, w.getBlock(x0, yN, z0)))
				{
					yN--; dist0++;
					if (yN <= 0) break;
				}
				if (w.getBlock(x0, yN, z0) == Blocks.air && dist0 < maxBranch)
				{
					w.setBlock(x0, yN, z0, this);
					if (!p.capabilities.isCreativeMode && --is0.stackSize <= 0)	p.inventory.setInventorySlotContents(p.inventory.currentItem, null);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Whether the block can be placed at this x,y,z. Checks the height of support members, or
	 * current size of the branches.
	 */
	public boolean canPlaceBlockAt(World w, int x0, int y0, int z0)
    {
        int dist0 = updateDist(w, x0, y0, z0, this);
        if (dist0 == 0)
        {
        	int i = 0;
        	//Move down to the bottom of the stack
        	while (w.getBlock(x0, y0 - 1, z0) instanceof BlockScaffold)
        	{
        		y0--;
        		i++;
        	}
        	
        	System.out.println(x0 + ", " + y0 + ", " + z0 + ", " + w.getBlock(x0, y0, z0).getClass().getSimpleName());
        	return (w.getBlock(x0, y0, z0) instanceof BlockScaffold == false) || (i <= ((BlockScaffold)w.getBlock(x0, y0, z0)).maxHeight);
        	
        }
        return (dist0 < maxBranch);
    }
	
	
	/**
	 * Let the player climb the scaffolding. Player has to be within the bounding box of a regular
	 * block, which means the bounding box has to be smaller than usual.
	 */
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
	{
		if (entity instanceof EntityPlayerMP)
		{
			NetHandlerPlayServer nethandler = ((EntityPlayerMP) entity).playerNetServerHandler;
			try {
				NetHandlerPlayServer.class.getDeclaredField("floatingTickCount").setInt(nethandler, 0);
			} catch (Exception e) {}
		}
		entity.fallDistance = 0;
		if (entity.isCollidedHorizontally && entity.isSneaking() == false) entity.motionY = 0.17D + ((double)Math.min(2, tier)) * 0.03D;
		else if (entity.isSneaking())
		{
			double diff = entity.prevPosY - entity.posY;
			//We have to move the bounding box before moving the player, otherwise we get illegal stance
			entity.boundingBox.minY += diff;
			entity.boundingBox.maxY += diff;
			entity.posY = entity.prevPosY;
			//This should solve the "can't shift stay" bug
			entity.motionY = 0;
		}
		else  entity.motionY = -0.10D;
	}
	
	/* Allow the blocks to random tick and update themselves. This should eventually cause any
	 * freak accidents to rectify themselves over time.
	 */
	public void updateTick(World w, int x0, int y0, int z0, Random rand) 
	{
		onNeighborBlockChange(w, x0, y0, z0, null);
	}
	
	public void onNeighborBlockChange(World w, int x0, int y0, int z0, Block bC)
	{
		//First, let's find our expected block distance.
		BlockScaffold b0 = getBaseBlock(this);
		
		int dist0 = updateDist(w, x0, y0, z0, b0),
			m0 = w.getBlockMetadata(x0, y0, z0),
			_dist0 = getActualValue(this, m0);
		
		if (dist0 > maxBranch)
		{
			w.setBlockToAir(x0, y0, z0);
			w.spawnEntityInWorld(new EntityItem(w, (double) x0 + 0.5D,(double) y0 + 0.5, (double) z0 + 0.5D, new ItemStack(getBaseBlock(this), 1)));
			return;
		}
		if (dist0 > _dist0)	
		{
			w.setBlockToAir(x0, y0, z0);
			w.spawnEntityInWorld(new EntityItem(w, (double) x0 + 0.5D,(double) y0 + 0.5, (double) z0 + 0.5D, new ItemStack(getBaseBlock(this), 1)));
		}
		else if (dist0 < _dist0) w.setBlock(x0, y0, z0, getBlockFromValue(b0, dist0), dist0 & 0xF, 1);
	}
	
	public void onBlockAdded(World w, int x0, int y0, int z0)
	{
		/*
		 * When the block is added, we update its distance.
		 * This also updates the block and its neighbors. Because of this, the player can place any
		 * branch tier, and it should just update itself and become a part of the scaffold structure.
		 * 
		 * Note: players should not accidentally get branch tier blocks.
		 */
		BlockScaffold b0 = getBaseBlock(this);
		
		int m0 = w.getBlockMetadata(x0, y0, z0);
		int dist0 = updateDist(w, x0, y0, z0, b0);
		if (dist0 < 0 || dist0 > maxBranch) 
		{
			w.setBlock(x0,y0,z0,Blocks.air,0,3);
			EntityItem ie = new EntityItem(w, (double) x0 + 0.5D,(double) y0 + 0.5, (double) z0 + 0.5D, new ItemStack(getBaseBlock(this), 1));
			w.spawnEntityInWorld(ie);
		}
		else w.setBlock(x0, y0, z0, getBlockFromValue(b0, dist0), dist0 & 0xF, 0);
	}
	
	 
	public boolean removedByPlayer(World w, EntityPlayer player, int x0, int y0, int z0, boolean willHarvest)
    {
		/*
		 * Essentially, when the player breaks a scaffold block, we walk down the branch and break the items.
		 * After that, we count how many were destroyed, and drop them in item stacks at the broken block.
		 */
		int dist0 = getActualValue(this, w.getBlockMetadata(x0,y0,z0));
		int total = collectStack(w, x0, y0, z0, dist0);
		if (player.capabilities.isCreativeMode == false)
		{
			while (total > 0)
			{
				int take; total -= (take = Math.min(64, total));
				EntityItem ie =  new EntityItem(w, (double) x0 + 0.5D,(double) y0 + 0.5, (double) z0 + 0.5D, new ItemStack(getBaseBlock(this), take));
				ie.motionY = 0.3; //throw the stack up in the air so that the player can catch it
				w.spawnEntityInWorld(ie);
			}
		}
		return false;
    }

	 
	/**
	 * The main update loop. Checks if we can stay, attempts to push changes to neighbors, etc
	 * 
	 * Works by stepping down the meta of the blocks by 1, as they go further from the support member. Then we
	 * can easily navigate around the structure by following the metadata direction.
	 * 
	 * It's a bit complicated, but it will give some really cool freedom for things later on.
	 * 
	 * @param w
	 * @param x0
	 * @param y0
	 * @param z0
	 * @param b0
	 * @param dist0 original dist, set to 
	 * @return the new dist of this block
	 */
	public static int updateDist(World w, int x0, int y0, int z0, BlockScaffold b0)
	{
		int x1, y1, z1, m1, dist0 = 0x7FFFFFFF;
		Block _b1; BlockScaffold b1;
		for (int i = 0; i < 6; i++)
		{
			x1 = x0 + Util.intDir[i][0]; y1 = y0 + Util.intDir[i][1]; z1 = z0 + Util.intDir[i][2];
			_b1 = w.getBlock(x1, y1, z1); 
			if (isSameScaffold(b0, _b1))
			{
				m1 = w.getBlockMetadata(x1, y1, z1);
				int dist1 = getActualValue((BlockScaffold)_b1, m1);
				if (i == 0 && dist1 == 0) return 0;
				dist0 = Math.min(dist0, dist1 + 1);
			}
			else if (_b1 instanceof BlockScaffold == false && i == 0 && _b1.isSideSolid(w, x1, y1, z1, ForgeDirection.UP)) return 0;
		}
		return dist0;
	}

	
	/**
	 * This algorithm walks through scaffold and destroys all the blocks it finds. Quite efficient.
	 * FIXME: Make blocks stay when scaffold is broken. How: Find all other nearby support members and push their distance out to the max range?
	 * 
	 * @param w
	 * @param x0
	 * @param y0
	 * @param z0
	 * @param dist0
	 * @return the number of blocks destroyed
	 */
	public int collectStack(World w, int x0, int y0, int z0, int dist0)
	{
		//Dun throwing block update
		w.setBlock(x0,y0,z0,Blocks.air,0,2);
		//First, let's go up the stack one by one :D
		int total = 1, x1, y1, z1;
		Block _b1;
		for (int i = 0; i < 6; i++)
		{
			x1 = x0 + Util.intDir[i][0]; y1 = y0 + Util.intDir[i][1]; z1 = z0 + Util.intDir[i][2];
			_b1 = w.getBlock(x1, y1, z1); 
			if (isSameScaffold(this, _b1))
			{
				int dist1 = getActualValue((BlockScaffold)_b1, w.getBlockMetadata(x1, y1, z1));
				//Strange bugs are afoot
				if (dist1 > dist0 || (i != 0 && dist1 == 0 && dist0 == 0)) total += ((BlockScaffold) _b1).collectStack(w, x1, y1, z1, dist1);
			}
			else if (_b1 instanceof BlockScaffold == false) w.notifyBlockOfNeighborChange(x1, y1, z1, this);
		}
		return total;
	}
	
	///////////////Some general util functions//////////////////////
	
	
	public static boolean isBlockSupportedByOtherSupports(World w, BlockPos pos0)
	{
		return false;
	}
	
	 /** Recurse through scaffold structure looking for support pillars.
     *  
     * @param w
     * @param pos0
     * @param flagged
     * @param posGridOrigin
     * @return
     */
    public static boolean doRecursiveWalk(World w, BlockPos pos0, BlockScaffold b0, HashSet<BlockPos> visited, int x, int z)
    {
    	boolean result = false;
    	BlockPos posN;
    	for (int i = 0; i < 6; i++)
    	{
    		posN = pos0.offset(i);
    		if (posN.pos[1] > 0 && posN.pos[1] < 256 && visited.contains(posN) == false)
    		{
    			visited.add(posN);
    			Block bN = posN.getBlock(w);
    			int m = posN.getBlockMetadata(w);
    			if (m == 0)
    			{
    				//Compare to the origin pillar!
    				if (posN.pos[0] != x && posN.pos[2] != z) return true;
    			}
    			if (isSameScaffold(b0, bN)) result = doRecursiveWalk(w, posN, b0, visited, x, z);
    		}
    	}
    	return false;
    }
	
	public static boolean isSameScaffold(BlockScaffold b0, Block b1)
	{
		return b1 instanceof BlockScaffold && getBaseBlock(b0) == getBaseBlock((BlockScaffold)b1);
	}
	
	@Override
    public boolean isBlockSolid(IBlockAccess world, int x, int y, int z, int side)
    {
        return side < 2;
    }
	
	/**
	 * Returns the actual distance value from the given block and metadata
	 * @param b
	 * @param m
	 * @return
	 */
	public static int getActualValue(BlockScaffold b, int m)
	{
		return m + (b.branchElement << 4);
	}
	
	/**
	 * Returns the expected block, from the base block and distance
	 * @param b
	 * @param m
	 * @return
	 */
	public static BlockScaffold getBlockFromValue(BlockScaffold b, int dist)
	{
		return (BlockScaffold) Module_Scaffolds.scaffolds.get(b)[dist >> 4];
	}
	
	/**
	 * Gets the base block for this scaffold system (converts stacked metadata downwards)
	 * @param b
	 * @return
	 */
	public static BlockScaffold getBaseBlock(BlockScaffold b)
	{
		return (BlockScaffold) Module_Scaffolds.scaffolds.get(b)[0];
	}
	
	
	/////////RENDERING DETAILS///////////

	@Override
	public boolean renderAsNormalBlock()
	{
		return true;
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
    @Override
    @SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockAccess e, int x, int y, int z, int side)
    {
        return true;
    }
}