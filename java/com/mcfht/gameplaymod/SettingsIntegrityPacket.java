package com.mcfht.gameplaymod;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class SettingsIntegrityPacket  implements IMessage
{
    

    public SettingsIntegrityPacket() { }


    @Override
    public void fromBytes(ByteBuf buf) 
    {
    	Settings.HP_BASE_AMOUNT = buf.readInt();
    	Settings.HP_BASE_GAIN = buf.readInt();
    	Settings.HP_BASE_LEVEL = buf.readInt();
    	Settings.HP_BASE_MAX = buf.readInt();
    	Settings.HP_BASE_STEP = buf.readInt();
    	
    	Settings.HP_BONUS_GAIN = buf.readInt();
    	Settings.HP_BONUS_LEVEL = buf.readInt();
    	Settings.HP_BONUS_MAX = buf.readInt();
    	Settings.HP_BONUS_STEP = buf.readInt();
    	
    	Settings.HP_REGAIN = buf.readFloat();
    	
    	Settings.doHungerPenalty = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) 
    {
    	buf.writeInt(Settings.HP_BASE_AMOUNT);
    	buf.writeInt(Settings.HP_BASE_GAIN);
    	buf.writeInt(Settings.HP_BASE_LEVEL);
    	buf.writeInt(Settings.HP_BASE_MAX);
    	buf.writeInt(Settings.HP_BASE_STEP);
    	
    	buf.writeInt(Settings.HP_BONUS_GAIN);
    	buf.writeInt(Settings.HP_BONUS_LEVEL);
    	buf.writeInt(Settings.HP_BONUS_MAX);
    	buf.writeInt(Settings.HP_BONUS_STEP);
    	
    	buf.writeFloat(Settings.HP_REGAIN);
    	
    	buf.writeBoolean(Settings.doSpecialRespawn);
    }


    public static class HandlerClient implements IMessageHandler<SettingsIntegrityPacket, IMessage> 
    {
        @Override
        public IMessage onMessage(SettingsIntegrityPacket message, MessageContext ctx) 
        {
            return null;
        }
    }
}



