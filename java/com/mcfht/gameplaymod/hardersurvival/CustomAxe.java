package com.mcfht.gameplaymod.hardersurvival;

import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemTool;

public class CustomAxe extends ItemAxe
{
	public CustomAxe(ToolMaterial m) 
	{
		super(m);
		this.efficiencyOnProperMaterial = 1F;
	}
	
	
	@Override
    public boolean func_150897_b(Block b)
    {
        return b.getMaterial() == Material.wood;
    }
}
