package com.mcfht.gameplaymod.hardersurvival;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mcfht.gameplaymod.ModCore;
import com.mcfht.gameplaymod.Settings;
import com.mcfht.gameplaymod.Util;
import com.mcfht.gameplaymod.playerdata.PlayerData;
import com.mcfht.gameplaymod.playerdata.PlayerData.Stats;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.registry.GameRegistry;

public class Module_Survival {

	public static Map<Object, Integer> harvesting;
	
	public static Item flintHatchet = (Item) new CustomAxe(Item.ToolMaterial.WOOD).setMaxDamage(40).setUnlocalizedName("flintHatchet").setTextureName(ModCore.MODID + ":flinthatchet");
	
	public Module_Survival()
	{
		harvesting = new HashMap<Object, Integer>();
		harvesting.put(Material.wood, 4);
		harvesting.put(Material.rock, 2);
		GameRegistry.registerItem(flintHatchet, "flintHatchet");
		GameRegistry.addShapedRecipe(new ItemStack(flintHatchet, 1), new Object[]{ "SF", "S ", 'S', Items.stick, 'F', Items.flint});
		
		
		List<Object> toRemove = new ArrayList<Object>();
		
		toRemove.add(Blocks.furnace);
		toRemove.add(Items.stone_axe);
		toRemove.add(Items.stone_pickaxe);
		toRemove.add(Items.stone_hoe);
		toRemove.add(Items.stone_shovel);
		toRemove.add(Items.stone_sword);
		
		Util.removeRecipes(toRemove);

		GameRegistry.addShapedRecipe(new ItemStack(Items.stone_axe), new Object[]{"FF ", "FS ", " S ", 'F', Items.flint, 'S', Items.stick});
		GameRegistry.addShapedRecipe(new ItemStack(Items.stone_pickaxe), new Object[]{"FFF", " S ", " S ", 'F', Items.flint, 'S', Items.stick});
		GameRegistry.addShapedRecipe(new ItemStack(Items.stone_hoe), new Object[]{"FF ", " S ", " S ", 'F', Items.flint, 'S', Items.stick});
		GameRegistry.addShapedRecipe(new ItemStack(Items.stone_shovel), new Object[]{"F", "S", "S", 'F', Items.flint, 'S', Items.stick});
		GameRegistry.addShapedRecipe(new ItemStack(Items.stone_sword), new Object[]{"F", "F", "S", 'F', Items.flint, 'S', Items.stick});

		//Register new furnace recipe
		GameRegistry.addShapedRecipe(new ItemStack(Blocks.furnace), new Object[]{"SCS","C C","SCS",'S', Blocks.cobblestone, 'C', Blocks.hardened_clay});
		GameRegistry.addShapedRecipe(new ItemStack(Blocks.furnace), new Object[]{"SCS","C C","SCS",'S', Blocks.cobblestone, 'C', Blocks.brick_block});
		GameRegistry.addShapedRecipe(new ItemStack(Blocks.furnace), new Object[]{"SCS","C C","SCS",'S', Blocks.hardened_clay, 'C', Blocks.cobblestone});
		GameRegistry.addShapedRecipe(new ItemStack(Blocks.furnace), new Object[]{"SCS","C C","SCS",'S', Blocks.brick_block, 'C', Blocks.cobblestone});
	
	}
	
	public static boolean canBreakByHand(Block b)
	{
		if (b.getMaterial().isToolNotRequired() == false) return false;
		Integer i = harvesting.get(b);
		if (i == null)
		i = harvesting.get(b.getClass());
		if (i == null) i = harvesting.get(b.getClass());
		if (i == null) i = harvesting.get(b.getMaterial());
		if (i == null) return true;
		return (i & 0x1) == 1;
	}
	
	public void sleepInBed(PlayerSleepInBedEvent e)
	{
		if (Settings.HARDER_BEDS == false) return;
		//Test a much larger area to search for mobs, blocks that can see the sky, blocks on which mobs could spawn, etc
	}
	
	@SubscribeEvent
	public void playerBreakingBlock(BreakSpeed e)
	{
		if (e.entity instanceof EntityPlayer)
		{
			int level;
			if (e.entity instanceof EntityPlayerMP)
			{
				PlayerData.loadStats(((EntityPlayerMP)e.entity).getDisplayName());
				level = PlayerData.getStat(Stats.LEVEL);
				if (level >= 0) level = ((EntityPlayerMP)e.entity).experienceLevel;
			}
			else
			{
				level = PlayerData.actualPlayerExperience;
				if (level >= 0) level = ((EntityPlayer)e.entity).experienceLevel;
			}
			
			if (level < 0) e.newSpeed *= (1F - ((float) Math.sqrt(-level)) / 20F);
			else e.newSpeed *= (1F + ((float) Math.sqrt(level)) / 30F);
			
			//Make da player 'ungry for dem block breakin
			e.entityPlayer.addExhaustion(0.02F);
			ItemStack is = e.entityPlayer.getHeldItem();
			
			//Get harvesting stats of held item
			int canWood = -1;
			int canRock = 0;
			int canDirt = -1;
			if (is != null)
			{
				canRock = is.getItem().getHarvestLevel(is, "pickaxe");
				if (is.getItem().getUnlocalizedName(is).contains("ood") == false)
				{
					canWood = is.getItem().getHarvestLevel(is, "axe");
				}
				canDirt = is.getItem().getHarvestLevel(is, "shovel");
			}

			if (e.block.getMaterial() == Material.wood)
			{
				if (canWood < 0)
				{
					e.newSpeed = 0;
					e.setCanceled(true);
				}
				else
				{
					e.newSpeed *= 0.7F + (float)(canWood)/3F;
				}
			}
			else if ((e.block.getMaterial() == Material.rock || e.block.getMaterial() == Material.iron))
			{
				if (canRock <= 0 || e.block.getHarvestLevel(e.metadata) > canRock)
				{
					e.newSpeed = 0;
					e.setCanceled(true);
				}
				else
				{
					e.newSpeed *= 0.5F + (canRock/3F);
				}
			}
			else if ((e.block.getMaterial() == Material.grass || e.block.getMaterial() == Material.ground || e.block.getMaterial() == Material.sand))
			{
				if (canDirt < 0)
				{
					e.newSpeed *= 0.5F;
				}
			}
		
			if (is != null && is.getItem() != null && is.getItem().getToolClasses(is).isEmpty() == false)
			{
				if (Util.rand.nextInt(8) == 0) is.damageItem(1, e.entityPlayer);
			}
			
			/*else if(e.entityPlayer.getHeldItem().getItem().
			 * {}
			 */
			/*
			else if (e.block.getMaterial() == Material.wood && e.entityPlayer.getHeldItem().getItem() instanceof ItemAxe == false)
			{
				e.setCanceled(true);
				e.newSpeed = 0;
			}
			else if((e.block.getMaterial() == Material.rock || e.block.getMaterial() == Material.iron) && (e.entityPlayer.getHeldItem().getItem() instanceof ItemPickaxe == false || e.entityPlayer.getHeldItem().getItem().getUnlocalizedName().contains("ood")))
			{
				e.setCanceled(true);
				e.newSpeed = 0;
			}*/
		}
	}
	
}
